from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader, RequestContext
import os, pdb
import sweetify
from datetime import datetime, timedelta
import numpy as np
import subprocess as sp
import MySQLdb 
import re
import time
import smtplib 
import threading

from operator import itemgetter
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect, reverse
from django.shortcuts import get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone

from twilio.rest import Client  
from django.views import generic
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.db.models import Count

from .models import MasterCategory, MasterSubCategory, KraCategoryTransaction, KraSubCategoryTransaction
from .models import LearningSkill, SharingSkill, CompetenceTransaction
from .models import Award, Commitment, Feedback
from .models import Associate, Department, Designation, Project, Hierarchy, AuthorityControl, Jira, Category, Ratings

# from flask import Flask, request, render_template
from django.views.generic import TemplateView
from chartjs.views.lines import BaseLineChartView
from .fusioncharts import FusionCharts


# for login purpose
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Pull the data from JIRA
from jira import JIRA
import jira.client
from jira.client import JIRA
from jira.exceptions import JIRAError

# Basic Libraries
import calendar
import math

# Google Speech to Text
import speech_recognition as sr

# Message info
from django.contrib.messages import get_messages
from django.contrib import messages

#-----------------------------------------------------------------------------------------------#
# USER LOGIN
#-----------------------------------------------------------------------------------------------#

def user_login(request):
    invalid = None
    active = None
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username = username, password = password)
        next = request.POST.get('next')
        if user:
            if user.is_active:
                login(request,user)
                if next:
                    return redirect('competence:dashboard')
                else:
                    return HttpResponseRedirect(reverse('competence:dashboard'))
            else:
                active = 'TRUE'
                context = {'next':next, 'invalid':invalid,'active':active}
                return render(request, 'registration/login.html', context )
        else:
            invalid = 'TRUE'
            print("Someone tried to login and failed.")
            print("They used username: {} and password: {}".format(username, password))
            context = {
                'next': next, 
                'invalid': invalid,
                'active': active}
            return render(request, 'registration/login.html', context)
    else:
        #Nothing has been provided for username or password.
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('competence:dashboard'))
        else:
            next = request.GET.get('next')
            context = {'next':next, 'invalid':invalid,'active':active}
            if next:
                return render(request, 'registration/login.html', context)
            else:
                return render(request, 'registration/login.html')
                
def change_password(request):
    try:
        login = Associate.objects.get(associate_id = request.user)
    except:
        login = {}
    context = {'login': login}
    return render(request, 'registration/change_password.html', context)

def update_password(request):
    invalid = None
    user = authenticate(username = request.POST['username'], password = request.POST['password'])
    if not user:
        error = 'Username or Password do not match'
        invalid = 'TRUE'
    elif request.POST['new_password'] == '' or request.POST['confirm_password'] == '':
        error = 'New Password or Confirm Password cannot be blanks'
        invalid = 'TRUE'
    elif request.POST['new_password'] != request.POST['confirm_password']:
        error = 'New Password and Confirm Password do not match'
        invalid = 'TRUE'

    if invalid:
        context = {
            'invalid': invalid,
            'error': error}
        return render(request, 'registration/change_password.html', context)
    else:
        # update the New Password
        user = User.objects.get(username = request.POST['username'])
        user.set_password(request.POST['new_password'])
        user.save()
        return HttpResponseRedirect(reverse('competence:user_login'))

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('competence:user_login'))

#-----------------------------------------------------------------------------------------------#
# GOOGLE SPEECH TO TEXT
#-----------------------------------------------------------------------------------------------#

@login_required
def speech_to_text(request):
    r = sr.Recognizer()                                                                                   
    with sr.Microphone() as source:                                                                       
        print("Say Something:")                                                                                   
        audio = r.listen(source)   
 
    try:
        print("You said: \n" + r.recognize_google(audio))

    except sr.UnknownValueError:
        print("Could not understand your speech. Please try again")

    except sr.RequestError as e:
        print("Could not request results; {0}".format(e))

#-----------------------------------------------------------------------------------------------#
# SCHEDULE JOBS
#-----------------------------------------------------------------------------------------------#

# @background(schedule=5)
# def schedule_job():
#     print ("Running", str(datetime.now()))

# schedule_job(schedule=5)


# schedule.every(5).seconds.do(schedule_job)
# schedule.every().day.at('17:09').do(schedule_job)

# while True:
#     schedule.run_pending()
#     time.sleep(20)

# t1 = threading.Thread(target=schedule_job)
# t1.start()
# t1.join

# To clear all schedule functions
# schedule.clear()

#-----------------------------------------------------------------------------------------------#
# SEND SMS USING TWILIO
#-----------------------------------------------------------------------------------------------#

def send_sms():
    accountSid = "AC70058fce2a1a567323bd74557bcd22c5"
    authToken = "535c6aefe2f494f3ba31bf372bc2bd68"
    client = Client(accountSid, authToken)
    message = client.messages.create(
                to="+918806535797", 
                from_="+12547915917",
                body="Hi Bhargav. This is Python!")

#-----------------------------------------------------------------------------------------------#
# SEND EMAIL - SMTPLIB
#-----------------------------------------------------------------------------------------------#

def send_email():  
    mail = smtplib.SMTP('smtp.outlook.com',587)
    mail.ehlo()
    mail.starttls()
    mail.login('bhargav.chelli@senecaglobal.com','07131A0209@Jun18')
    mail.sendmail('bhargav.chelli@senecaglobal.com', 'bhargav.chelli@senecaglobal.com', "Hello There from Python.")
    mail.close();

#-----------------------------------------------------------------------------------------------#
# SCHEDULE JOB
#-----------------------------------------------------------------------------------------------#

# @background(schedule=5)
# def schedule_job():
  #  pdb.set_trace()
  #  print ("Running", str(datetime.now()))
# schedule_job()

#-----------------------------------------------------------------------------------------------#
# MAIN MENU - DASHBOARD
#-----------------------------------------------------------------------------------------------#

@login_required 
def dashboard(request):
    role = 'Associate'
    context = manager_dashboard(request, request.user, role)
    return render(request, 'dashboard.html', context)

@login_required
def associate_dashboard(request, associate_id):
    role = 'Manager'
    if associate_id == ' ':
        try:
            associate = Associate.objects.filter(manager_id = request.user).order_by('associate_name').first()
            associate_id = associate.associate_id
        except:
            associate_id = request.user
    context = manager_dashboard(request, associate_id, role)
    return render(request, 'dashboard.html', context)

@login_required 
def manager_dashboard(request, associate_id, role):
    login = Associate.objects.get(associate_id = request.user)
    associates = Associate.objects.filter(manager_id = request.user).order_by('associate_name')
    selected_associate = Associate.objects.get(associate_id = associate_id)

    # Key Result Areas
    targets, achieved, kralabels = ([] for i in range(3))
    try:
        latest = KraCategoryTransaction.objects.filter(associate_id = associate_id, status="Close").latest('effective_end')
        effective_end = str(latest.effective_end)
        krasubtitle = get_current_adr_cycle()
        krasubtitlefontColor = "grey"
        categories = KraCategoryTransaction.objects.filter(associate_id = associate_id, effective_end = effective_end)
        for category in categories:
            targets.append(category.category_target)
            achieved.append(category.manager_rating)
            kralabels.append(category.category)
    except:
        effective_end = datetime.today().strftime('%Y-%m-%d')
        krasubtitle = 'No Data found from ' + get_current_adr_cycle()
        krasubtitlefontColor = "red"
        targets =  [3,1,2,3,5,5,5]
        achieved = [0,0,0,0,0,0,0] 

    # Productivity - Jira
    stories, avgstories, totaltasks, firsttimecorrect, ontimedelivery, rating, avgtasks, complianttasks, last12months, lastmonths, lastyears, appreciations, maxappreciations = ([] for i in range(13))
    totalmonths = datetime.now().year * 12 + datetime.now().month - 1 # Months since year 0 minus 1
    for i in range(12):
        month = (totalmonths - i) % 12 + 1
        year = (totalmonths - i) // 12
        year1 = str((totalmonths - i) // 12)[2:]
        if month == 0:
            month = 12
            year = year - 1

        lastmonths.append(month)
        lastyears.append(int(year1))
        # last12months.append(calendar.month_abbr[month] + "'" + str(year)[2:])

        # Get the Team's average Story points
        competencies = Jira.objects.filter(project_id = selected_associate.project_id, year = year, month = month)
        if competencies:
            for competency in competencies:
                if competency:
                    avgstories.append(round(competency.story_points,2))
                    avgtasks.append(round(competency.total_tasks,2))
                else:
                    avgstories.append(0)
                    avgtasks.append(0)
            try:            
                teamavgstories = round(np.mean(avgstories),2)
                teamavgtasks = round(np.mean(avgtasks),2)
            except:
                teamavgstories = 0
                teamavgtasks = 0
        else:
            teamavgstories = 0
            teamavgtasks = 0
        
        # Get each associate's story points, total tasks and due date compliant tasks
        competencies = Jira.objects.filter(jira_id = selected_associate.jira_id, year = year, month = month)
        if competencies:
            for competency in competencies:
                # stories.append(round(competency.story_points,2))
                stories.append(int(competency.story_points))
                totaltasks.append(competency.total_tasks)
                complianttasks.append((competency.due_compliant_tasks))
                firsttimecorrect.append(int(competency.firsttime_correct))
                ontimedelivery.append(int(competency.ontime_delivery))
                rating.append(int(competency.technical_competence_rating))
        else:    
            stories.append(0)
            totaltasks.append(0)
            complianttasks.append(0)
            ontimedelivery.append(100)
            firsttimecorrect.append(100)
            rating.append(0)
        try:            
            associateavgstories = round(np.mean(stories),2)
            associateavgtasks = round(np.mean(totaltasks),2)
        except:
            associateavgstories = 0
            associateavgtasks = 0
        
        # Appreciations
        startdate = str(year) + '-' + str(month).zfill(2) + '-01'
        if month == 12:
            month = 0
            year = year + 1
        enddate = str(year) + '-' + str(month+1).zfill(2) + '-01'
        count = Commitment.objects.filter(associate_id = associate_id, created_date__range=[startdate, enddate]).count()
        if count:
            appreciations.append(count)
        else:
            appreciations.append(0)
        
        # Get the Team's highest Appreciations on monthly basis
        highestappreciation = Commitment.objects.filter(project_id = selected_associate.project_id, created_date__range=[startdate, enddate]).values('associate_id').annotate(count=Count('associate_id'))
        if highestappreciation:
            maxappreciations.append(max([x['count'] for x in highestappreciation]))
        else:
            maxappreciations.append(0) 
    
    selected_cycle = get_current_financial_cycle()
    startdate = selected_cycle[3:7] + '-03-31'
    enddate = selected_cycle[10:14] + '-04-01'

    # COUNTS
    learning_count = LearningSkill.objects.filter(associate_id = associate_id, status = 'Close', created_date__range=[startdate, enddate]).count()
    sharing_count = SharingSkill.objects.filter(associate_id = associate_id, status = 'Close', created_date__range=[startdate, enddate]).count()

    # Awards - Scrollable
    awardsscroll = Award.objects.filter(associate_id = associate_id, created_date__range=[startdate, enddate], status = 'Approved') 

    # Appreciations - Scrollable
    appreciationsscroll = Commitment.objects.filter(associate_id = associate_id, created_date__range=[startdate, enddate], status = 'Approved') \
                                                .extra(
                                                select={'associate_name':
                                                'Select associate_name from Associate where '
                                                'Associate.associate_id = Commitment.recognised_by'}
                                                )

    status = ['In Progress', 'Action Approval', 'Close']
    # Feedbacks - Scrollable
    feedbacksscroll = Feedback.objects.filter(associate_id = associate_id, created_date__range=[startdate, enddate], status__in = status) \
                                          .extra(
                                          select={'associate_name':
                                          'Select associate_name from Associate where '
                                          'Associate.associate_id = Feedback.feedback_by'}
                                          )

    storiescount = sum(stories)
    if storiescount == 0:
        storiessubtitle = "No Data found"
        storiessubtitlefontColor = "red"
    else:
        storiessubtitle = "Last 12 months"
        storiessubtitlefontColor = "grey"

    if appreciationsscroll.count == 0:
        appsubtitle = "No Data found"
        appsubtitlefontColor = "red"
    else:
        appsubtitle = "Last 12 months"
        appsubtitlefontColor = "grey"

    # Ratings
    cycle = get_current_financial_cycle()
    selected_cycle = get_current_adr_cycle()
    # if selected_cycle[:3] == 'Apr' :
    #     rating_type = 'rating_kra_cycle1'
    # elif selected_cycle[:3] == 'Aug' :
    #     rating_type = 'rating_kra_cycle2'
    # else:
    #     rating_type = 'rating_kra_cycle3'
    try:
        ratings = Ratings.objects.get(associate_id = associate_id, cycle = cycle)
        # kra_rating = getattr(ratings, rating_type)
    except:
        ratings = {}

    totaltaskscount = sum(totaltasks)
    duetaskscount = sum(complianttasks)
    last12months.reverse() 
    lastmonths.reverse() 
    lastyears.reverse()
    stories.reverse()
    totaltasks.reverse()
    complianttasks.reverse()
    appreciations.reverse()
    maxappreciations.reverse()
    ontimedelivery.reverse()
    firsttimecorrect.reverse()
    rating.reverse()
    ontime_delivery = round(np.mean(ontimedelivery),2)
    firsttime_correct = round(np.mean(firsttimecorrect),2)

    # Find the average Competence rating of an Associate : 
    try:
        competency_rating = sum(rating) / sum(x > 0 for x in rating)
        competency_rating = round(np.mean(competency_rating),2)
    except:
        competency_rating = 'N/A'

    context = {
        'role': role,
        'associates': associates,
        'selected_associate': selected_associate,

        'login': login,
        'last12months': last12months, 
        'lastmonths': lastmonths,
        'lastyears': lastyears,

        'kralabels': kralabels, 
        'targets': targets, 
        'achieved':achieved, 
        'krasubtitle': krasubtitle,
        'krasubtitlefontColor': krasubtitlefontColor,

        'stories': stories, 
        'storiessubtitle': storiessubtitle,
        'teamavgstories': teamavgstories,
        'storiescount': storiescount,
        'associateavgstories': associateavgstories,
        'storiessubtitlefontColor': storiessubtitlefontColor,
        'ontime_delivery': ontime_delivery,

        'totaltasks': totaltasks, 
        'complianttasks': complianttasks,
        'teamavgtasks': teamavgtasks,
        'associateavgtasks': associateavgtasks,
        'totaltaskscount': totaltaskscount,
        'duetaskscount': duetaskscount,
        'ontimedelivery': ontimedelivery,
        'rating': rating,
        'competency_rating': competency_rating,
        'firsttime_correct': firsttime_correct,
        'firsttimecorrect': firsttimecorrect,

        'maxappreciations': maxappreciations,
        'appreciations': appreciations,
        'appsubtitle': appsubtitle,
        'appsubtitlefontColor': appsubtitlefontColor,

        'awardsscroll': awardsscroll,
        'appreciationsscroll': appreciationsscroll,
        'feedbacksscroll': feedbacksscroll,

        'ratings': ratings,

        'learning_count': learning_count,
        'sharing_count': sharing_count,
        }

    return context

#-----------------------------------------------------------------------------------------------#
# ADMIN - Associate Master Table
#-----------------------------------------------------------------------------------------------#

@login_required
def associate_master(request):
    alert = get_message(request)   
    departments = Department.objects.all().order_by('department_id')
    for department in departments:
        first_department_id = department.department_id
        break
    designations = Designation.objects.all().order_by('grade')
    for designation in designations:
        first_grade = designation.grade
        break
    projects = Project.objects.all().order_by('project_name')
    for project in projects:
        first_project_id = project.project_id
        break
    associates = Associate.objects.all().order_by('associate_id')
    associatecount = Associate.objects.count()
    managers = Associate.objects.filter(grade__in = ["G4-A", "G5"])
    skillset = ['Java', '.Net', 'Python', 'Angular', 'iSeries', 'Ruby']
    current_date = datetime.now().strftime("%Y/%m/%d")
    login = Associate.objects.get(associate_id = request.user)
    context = {
        'login' : login,
        'associates': associates,
        'managers': managers,
        'departments': departments,
        'first_department_id': first_department_id,
        'designations': designations,
        'first_grade': first_grade,
        'projects': projects,
        'first_project_id': first_project_id,
        'current_date': current_date,
        'skillset': skillset,
        'alert': alert
    }
    return render(request, 'admin/associate_master.html', context)

# Adds a new Associate
@login_required
def associate_add(request):
    manager = Associate.objects.get(associate_name = request.POST['manager_name'])
    associate = Associate(
        associate_id=request.POST['associate_id'],
        associate_name=request.POST['associate_name'],
        designation=request.POST['designation'],
        grade=request.POST['grade'],
        department_id=request.POST['department_id'],
        department_name=request.POST['department_name'],
        role=request.POST['role'],
        manager_name=request.POST['manager_name'],
        manager_id = manager.associate_id,
        project_id=request.POST['project_id'],
        project_name=request.POST['project_name'],
        date_of_join=request.POST['date_of_join'],
        email_address=request.POST['email_address'],
        jira_id=request.POST['jira_id'],
        primary_skills=request.POST['primary_skills'],
        created_by=request.user)
    associate.save()
    messages.info(request, 'added')
    user = User.objects.create_user(username = request.POST['associate_id'], 
                                    password = 'seneca@123',
                                    email = request.POST['email_address'])

    selected_cycle = get_current_adr_cycle()
    startdate, enddate = get_date_range(selected_cycle)

    # Populate the default KRAs to the newly added Associate 
    mastercategories = MasterCategory.objects.all()
    for mastercategory in mastercategories:
        kracategorytransaction = KraCategoryTransaction(
            associate_id = request.POST['associate_id'],
            associate_name = request.POST['associate_name'],
            category = mastercategory.category,
            weightage = mastercategory.weightage,
            category_target = mastercategory.target,
            associate_comments = ' ',
            manager_feedback = ' ',
            manager_rating = 0,
            status = 'Create',
            effective_start = startdate,
            effective_end = enddate,
            created_by = request.user)
        kracategorytransaction.save()

        mastersubcategories = MasterSubCategory.objects.filter(category = mastercategory.category)
        for mastersubcategory in mastersubcategories:
            krasubcategorytransaction = KraSubCategoryTransaction(
                associate_id = request.POST['associate_id'],
                associate_name = request.POST['associate_name'],
                category = mastersubcategory.category,
                subcategory = mastersubcategory.subcategory,
                subcategory_target = mastersubcategory.target,
                associate_comments = ' ',
                manager_feedback = ' ',
                manager_rating = 0,
                status = 'Create',
                effective_start = startdate,
                effective_end = enddate,
                created_by = request.user)
            krasubcategorytransaction.save()

    categories = Category.objects.all()
    for category in categories:
        competencetransaction = CompetenceTransaction(
            associate_id = request.POST['associate_id'],
            category_type = category.category_type,
            category = category.category,
            associate_comments = ' ',
            manager_feedback = ' ',
            manager_rating = 0,
            status = 'Open',
            created_by = request.user)
        competencetransaction.save()

    context = {'associate': associate}
    return redirect('/associate_master')

# View the details of an Associate
@login_required
def associate_detail(request, id):
    return redirect('/associate_master')

# Updates an Associate
@login_required
def associate_update(request, id):
    manager = Associate.objects.get(associate_name = request.POST['manager_name'])
    associate = Associate.objects.get(id=id)
    associate.associate_id = request.POST['associate_id']
    associate.associate_name = request.POST['associate_name']
    associate.designation = request.POST['designation']
    associate.grade = request.POST['grade']
    associate.department_id = request.POST['department_id']
    associate.department_name = request.POST['department_name']
    associate.role = request.POST['role']
    associate.manager_name = request.POST['manager_name']
    associate.manager_id = manager.associate_id
    associate.project_id = request.POST['project_id']
    associate.project_name = request.POST['project_name']
    associate.date_of_join = request.POST['date_of_join']
    associate.email_address = request.POST['email_address']
    associate.jira_id = request.POST['jira_id']
    associate.primary_skills = request.POST['primary_skills']
    associate.last_updated_by = str(request.user)
    associate.save()
    messages.info(request, 'updated')
    return redirect('/associate_master')

# Deletes an Associate and associated information
@login_required
def associate_delete(request, id):
    associate = Associate.objects.get(id=id)
    kracategory = KraCategoryTransaction.objects.filter(associate_id = associate.associate_id)
    krasubcategory = KraSubCategoryTransaction.objects.filter(associate_id = associate.associate_id)
    competencetransaction = CompetenceTransaction.objects.filter(associate_id = associate.associate_id)
    user = User.objects.get(username = associate.associate_id)
    associate.delete()
    kracategory.delete()
    krasubcategory.delete()
    user.delete()
    messages.info(request, 'deleted')
    return redirect('/associate_master')


#-----------------------------------------------------------------------------------------------#
# ADMIN - Hierarchy Master Table
#-----------------------------------------------------------------------------------------------#

@login_required
def hierarchy_master(request):
    alert = get_message(request)
    projects = Project.objects.all()
    hierarchies = Hierarchy.objects.all().extra(
    select = {'associate_name':
                'Select associate_name from Associate where '
                'Associate.associate_id = Hierarchy.associate_id ',
              'manager_id':
                'Select manager_id from Associate where '
                'Associate.associate_id = Hierarchy.associate_id ' ,
              'manager_name':
                'Select manager_name from Associate where '
                'Associate.associate_id = Hierarchy.associate_id ' ,  
              'project_id':
                'Select project_id from Associate where '
                'Associate.associate_id = Hierarchy.associate_id ',              
              'project_name':
                'Select project_name from Associate where '
                'Associate.associate_id = Hierarchy.associate_id '}
              )
    associates = Associate.objects.filter(manager_id = request.user).order_by('associate_name')
    access_levels = ['1', '2', '3', '4', '5']
    login = Associate.objects.get(associate_id = request.user)
    context = {
        'login' : login,
        'hierarchies': hierarchies,
        'projects': projects,
        'associates': associates,
        'access_levels': access_levels,
        'alert': alert
    }
    return render(request, 'admin/hierarchy_master.html', context)

# Adds a new Hierarchy
@login_required
def hierarchy_add(request):
    associate = request.POST['associate']
    associate_id = associate[associate.find('(')+1:associate.find(')')]
    hierarchy = Hierarchy(
        associate_id = associate_id,
        access_level = request.POST['access_level'],
        created_by = request.user)
    context = {'hierarchy': hierarchy}
    hierarchy.save()
    messages.info(request, 'added')
    hierarchies = Hierarchy.objects.all()
    context = {'hierarchies': hierarchies}
    return redirect('/hierarchy_master')

# Updates a Hierarchy
@login_required
def hierarchy_update(request, id):
    hierarchy = Hierarchy.objects.get(id=id)
    hierarchy.access_level = request.POST['access_level']
    hierarchy.last_updated_by = str(request.user)
    hierarchy.save()
    messages.info(request, 'updated')
    return redirect('/hierarchy_master')

# Deletes a Hierarchy
@login_required
def hierarchy_delete(request, id):
    hierarchy = Hierarchy.objects.get(id=id)
    hierarchy.delete()
    messages.info(request, 'deleted')
    return redirect('/hierarchy_master')

#-----------------------------------------------------------------------------------------------#
# ADMIN - Control Master Table
#-----------------------------------------------------------------------------------------------#

@login_required
def control_master(request):
    alert = get_message(request)
    controls = AuthorityControl.objects.all().extra(
    select = {'associate_name':
                'Select associate_name from Associate where '
                'Associate.associate_id = AuthorityControl.associate_id ',
              'manager_id':
                'Select manager_id from Associate where '
                'Associate.associate_id = AuthorityControl.associate_id ' ,
              'manager_name':
                'Select manager_name from Associate where '
                'Associate.associate_id = AuthorityControl.associate_id ' ,  
              'project_id':
                'Select project_id from Associate where '
                'Associate.associate_id = AuthorityControl.associate_id ',              
              'project_name':
                'Select project_name from Associate where '
                'Associate.associate_id = AuthorityControl.associate_id '}
              )
    associates = Associate.objects.filter(manager_id = request.user).order_by('associate_name')
    authority_levels = ['1', '2', '3', '4', '5']
    login = Associate.objects.get(associate_id = request.user)
    context = {
        'login' : login,
        'controls': controls,
        'associates': associates,
        'authority_levels': authority_levels,
        'alert': alert
    }
    return render(request, 'admin/control_master.html', context)

@login_required
def control_add(request):
    associate = request.POST['associate']
    associate_id = associate[associate.find('(')+1:associate.find(')')]
    control = AuthorityControl(
        associate_id = associate_id,
        menu_name = request.POST['menu_name'],
        link_name = request.POST['link_name'],
        authority = request.POST['authority'],
        created_by=request.user)
    context = {'control': control}
    control.save()
    messages.info(request, 'added')
    controls = AuthorityControl.objects.all()
    context = {'controls': controls}
    return redirect('/control_master')

@login_required
def control_update(request, id):
    control = AuthorityControl.objects.get(id=id)
    control.menu_name = request.POST['menu_name']
    control.link_name = request.POST['link_name']
    control.authority = request.POST['authority']
    control.last_updated_by = str(request.user)
    control.save()
    messages.info(request, 'updated')    
    return redirect('/control_master')

@login_required
def control_delete(request, id):
    control = AuthorityControl.objects.get(id=id)
    control.delete()
    messages.info(request, 'deleted')    
    return redirect('/control_master')

#-----------------------------------------------------------------------------------------------#
# ADMIN - Project Master Table
#-----------------------------------------------------------------------------------------------#

@login_required
def project_master(request):
    alert = get_message(request)
    projects = Project.objects.all().extra(
    select = {'manager_name':
              'Select associate_name from Associate where '
              'Associate.associate_id = Project.manager_id ',
              'lead_name':
              'Select associate_name from Associate where '
              'Associate.associate_id = Project.lead_id '
              })
    projectcount = Project.objects.count()
    managers = Associate.objects.filter(grade__in = ["G4-A", "G4-B", "G5-A", "G5-B"])
    leads = Associate.objects.filter(grade__in = ["G3-A", "G3-B"])  
    login = Associate.objects.get(associate_id = request.user)
    context = {
        'login' : login,
        'projects': projects,
        'managers': managers,
        'leads': leads,
        'alert': alert
    }
    return render(request, 'admin/project_master.html', context)

@login_required
def project_add(request):
    manager = request.POST['manager']
    manager_id = manager[manager.find('(')+1:manager.find(')')]
    lead = request.POST['lead']
    lead_id = lead[lead.find('(')+1:lead.find(')')]
    project = Project(
        project_name = request.POST['project_name'],
        project_id  = request.POST['project_id'],
        manager_id = manager_id,
        lead_id = lead_id,
        created_by = request.user)
    project.save()
    messages.info(request, 'added')
    return redirect('/project_master')

@login_required
def project_update(request, id):
    manager = request.POST['manager']
    manager_id = manager[manager.find('(')+1:manager.find(')')]
    lead = request.POST['lead']
    lead_id = lead[lead.find('(')+1:lead.find(')')]
    project = Project.objects.get(id=id)
    project.project_name = request.POST['project_name']
    project.manager_id = manager_id
    project.lead_id = lead_id
    project.last_updated_by = str(request.user)
    project.save()
    messages.info(request, 'updated')
    return redirect('/project_master')

@login_required
def project_delete(request, id):
    project = Project.objects.get(id=id)
    project.delete()
    messages.info(request, 'deleted')
    return redirect('/project_master')

#-----------------------------------------------------------------------------------------------#
# ADMIN - Department Master Table
#-----------------------------------------------------------------------------------------------#

@login_required
def department_master(request):
    alert = get_message(request)
    departments = Department.objects.all()
    departmentcount = Department.objects.count()
    login = Associate.objects.get(associate_id = request.user)
    context = {
        'login' : login,
        'departments': departments,
        'departmentcount': departmentcount,
        'alert': alert
    }
    return render(request, 'admin/department_master.html', context)

@login_required
def department_add(request):
    department = Department(
        department_name = request.POST['department_name'],
        department_id = request.POST['department_id'],
        created_by = request.user)
    department.save()
    messages.info(request, 'added')
    return redirect('/department_master')

@login_required
def department_update(request, id):
    department = Department.objects.get(id=id)
    department.department_name = request.POST['department_name']
    department.department_id = request.POST['department_id']
    department.last_updated_by = str(request.user)
    department.save()
    messages.info(request, 'updated')
    return redirect('/department_master')

@login_required
def department_delete(request, id):
    department = Department.objects.get(id=id)
    department.delete()
    messages.info(request, 'deleted')
    return redirect('/department_master')

#-----------------------------------------------------------------------------------------------#
# ADMIN - Designation Master Table
#-----------------------------------------------------------------------------------------------#

@login_required
def designation_master(request):
    alert = get_message(request)
    designations = Designation.objects.all()
    designationcount = Designation.objects.count()
    login = Associate.objects.get(associate_id = request.user)
    context = {
        'login' : login,
        'designations': designations,
        'designationcount': designationcount,
        'alert': alert
    }
    return render(request, 'admin/designation_master.html', context)

@login_required
def designation_add(request):
    designation = Designation(
        designation = request.POST['designation'],
        grade = request.POST['grade'],
        created_by = request.user)
    designation.save()
    messages.info(request, 'added')
    return redirect('/designation_master')

@login_required
def designation_update(request, id):
    designation = Designation.objects.get(id=id)
    designation.designation = request.POST['designation']
    designation.grade = request.POST['grade']
    designation.last_updated_by = str(request.user)
    designation.save()
    messages.info(request, 'updated')
    return redirect('/designation_master')

@login_required
def designation_delete(request, id):
    designation = Designation.objects.get(id=id)
    designation.delete()
    messages.info(request, 'deleted')
    return redirect('/designation_master')

#-----------------------------------------------------------------------------------------------#
# ADMIN - Category Master Table
#-----------------------------------------------------------------------------------------------#

@login_required
def kracategory_master(request):
    alert = get_message(request)
    masterc = MasterCategory.objects.all()
    mastersubc = MasterSubCategory.objects.all()
    login = Associate.objects.get(associate_id = request.user)
    context = {
        'login' : login,
        'masterc': masterc, 
        'mastersubc': mastersubc,
        'alert': alert
        }
    return render(request, 'admin/kracategory_master.html', context)

# Adds a record to the MasterCategory table
@login_required
def kracategory_add(request):
    masterc = MasterCategory(
        category = request.POST['category'],
        subcategory_count = 0,
        description = request.POST['description'],
        weightage = request.POST['weightage'],
        target = request.POST['target'],
        created_by = request.user)
    context = {'masterc': masterc}
    masterc.save()

    currentYear = datetime.now().year
    # Populate the KRA Txn files with the new Category/Subcategory 
    associates = Associate.objects.filter(manager_id = request.user).order_by('associate_name')
    for associate in associates:
        kracategorytransaction = KraCategoryTransaction(
            associate_id = associate.associate_id,
            associate_name = associate.associate_name,
            category = request.POST['category'],
            weightage = request.POST['weightage'],
            category_target = request.POST['target'],
            associate_comments = " ",
            manager_feedback = " ",
            manager_rating = 0,
            status = 'Open',
            effective_start = str(currentYear) + '-04' + '-01',
            effective_end = str(currentYear + 1) + '-03' + '-31',
            created_by = request.user)
        kracategorytransaction.save()
    
    messages.info(request, 'added')
    context = {'masters': masterc}
    return redirect('/kracategory_master')

# Adds a record to the MasterSubCategory table
@login_required
def krasubcategory_add(request, category):
    mastersubc = MasterSubCategory(
        category = request.POST['category'],
        subcategory = request.POST['subcategory'],
        description = request.POST['description'],
        target = request.POST['target'],
        created_by = request.user)
    context = {'mastersubc': mastersubc}
    mastersubc.save()

    # Update the sub categories count to MasterCatogory table
    count = MasterSubCategory.objects.filter(category=category).count()
    masterc = MasterCategory.objects.get(category = category)
    masterc.subcategory_count = count
    masterc.save()

    # Populate the KRA Txn files with the new Category/Subcategory 
    associates = Associate.objects.filter(manager_id = request.user).order_by('associate_name')
    for associate in associates:
        krasubcategorytransaction = KraSubCategoryTransaction(
            associate_id = associate.associate_id,
            associate_name = associate.associate_name,
            category = request.POST['category'],
            subcategory = request.POST['subcategory'],
            subcategory_target = request.POST['target'],
            associate_comments = " ",
            manager_feedback = " ",
            manager_rating = 0,
            status = 'Open',
            effective_start = str(datetime.now().year) + '-04' + '-01',
            effective_end = str(datetime.now().year + 1) + '-03' + '-31',
            created_by = request.user)
        krasubcategorytransaction.save()

    messages.info(request, 'added')      
    return redirect('/kracategory_master')

# Updates a row in MasterCategory table
@login_required
def kracategory_update(request, id):
    masterc = MasterCategory.objects.get(id=id)
    masterc.description = request.POST['description']
    masterc.weightage = request.POST['weightage']
    masterc.target = request.POST['target']
    masterc.last_updated_by = str(request.user)
    masterc.save()
    messages.info(request, 'updated')
    return redirect('/kracategory_master')

# Deletes a row in MasterCategory table
@login_required
def kracategory_delete(request, id):
    masterc = MasterCategory.objects.get(id=id)
    mastersubc = MasterSubCategory.objects.filter(category = masterc.category)
    deletekracategory = KraCategoryTransaction.objects.filter(category = masterc.category, effective_start = str(datetime.now().year) + '-04' + '-01')
    masterc.delete()
    mastersubc.delete()
    deletekracategory.delete()
    messages.info(request, 'deleted')
    return redirect('/kracategory_master')

#-----------------------------------------------------------------------------------------------#
# ADMIN - Sub Category Master Table
#-----------------------------------------------------------------------------------------------#

@login_required
def krasubcategory_master(request, category):
    alert = get_message(request)
    mastersubc = MasterSubCategory.objects.filter(category=category).values()
    login = Associate.objects.get(associate_id = request.user)
    context = {
        'login' : login,
        'mastersubc': mastersubc,
        'alert': alert
        }
    return render(request, 'admin/krasubcategory_master.html', context)

# Updates a row in MasterSubCategory table
@login_required
def krasubcategory_update(request, id):
    mastersubc = MasterSubCategory.objects.get(id=id)
    category = mastersubc.category
    mastersubc.subcategory = request.POST['subcategory']
    mastersubc.description = request.POST['description']
    mastersubc.target = request.POST['target']
    mastersubc.created_by = str(request.user)
    mastersubc.save()
    messages.info(request, 'updated')
    return redirect('/krasubcategory_master/' + category)

# Deletes a row in MasterSubCategory table
@login_required
def krasubcategory_delete(request, id):
    mastersubc = MasterSubCategory.objects.get(id=id)
    category = mastersubc.category
    deletekrasubcategory = KraSubCategoryTransaction.objects.filter(subcategory = mastersubc.subcategory, effective_start = str(datetime.now().year) + '-04' + '-01')
    deletekrasubcategory.delete()
    mastersubc.delete()
    messages.info(request, 'deleted')

    # Update the sub categories count to MasterCatogory table
    count = MasterSubCategory.objects.filter(category = mastersubc.category).count()
    masterc = MasterCategory.objects.get(category = mastersubc.category)
    masterc.subcategory_count = count
    masterc.save()
    request.user = str(request.user)
    return redirect('/krasubcategory_master/' + category)

#-----------------------------------------------------------------------------------------------#
# ADMIN - Other Category Master Table
#-----------------------------------------------------------------------------------------------#

@login_required
def category_master(request):
    alert = get_message(request)
    categories = Category.objects.all()
    login = Associate.objects.get(associate_id = request.user)
    context = {
        'login' : login,
        'categories': categories,
        'alert': alert
    }
    return render(request, 'admin/category_master.html', context)

@login_required
def category_add(request):
    category = Category(
        category_type = request.POST['category_type'],
        category = request.POST['category'],
        created_by = request.user)
    category.save()
    messages.info(request, 'added')
    return redirect('/category_master')

@login_required
def category_update(request, id):
    category = Category.objects.get(id=id)
    category.category_type = request.POST['category_type']
    category.category = request.POST['category']
    category.last_updated_by = str(request.user)
    category.save()
    messages.info(request, 'updated')
    return redirect('/category_master')

@login_required
def category_delete(request, id):
    category = Category.objects.get(id=id)
    category.delete()
    messages.info(request, 'deleted')
    return redirect('/category_master')

#-----------------------------------------------------------------------------------------------#
# MANAGER - Copy Competencies 
#-----------------------------------------------------------------------------------------------#
    
@login_required
def copy_competencies(request):
    associates = Associate.objects.filter(manager_id = request.user).order_by('associate_name')
    for associate in associates:
        categories = Category.objects.all()
        for category in categories:
            competencetransaction = CompetenceTransaction(
                associate_id = associate.associate_id,
                category_type = category.category_type,
                category = category.category,
                associate_comments = ' ',
                manager_feedback = ' ',
                manager_rating = 0,
                status = 'Open',
                effective_start = request.POST['selected_start'],
                effective_end = request.POST['selected_end'],
                created_by = request.user,
                last_updated_by = request.user)
            competencetransaction.save()
    return render(request, 'manager/manager_learning.html', context)

#-----------------------------------------------------------------------------------------------#
# MANAGER - Learning Skills
#-----------------------------------------------------------------------------------------------#

@login_required
def manager_learning(request, associate_id, selected_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, associate_id, selected_cycle, 'financial')
    alert = get_message(request)
    try:
        status = ['Pending Approval','Close']
        learning_skills = LearningSkill.objects.filter(associate_id = associate_id, status__in = status, created_date__range=[startdate, enddate]).extra(
                        select={'associate_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = LearningSkill.associate_id',
                                'project_id':
                                'Select project_id from Associate where '
                                'Associate.associate_id = LearningSkill.associate_id',
                                'project_name':
                                'Select project_name from Associate where '
                                'Associate.associate_id = LearningSkill.associate_id'}
                                )

    except:
        learning_skills = {}
        startdate = 0
        enddate = 0

    cdps_awarded = ['1', '2', '3', '4', '5']  
    category = 'Learning Skills'

    # Get the Competence Pending Counts
    learning_pendingcount, sharing_pendingcount = get_pending_competence(associate_id, startdate, enddate)

    context = {
        'login' : login,
        'associate_id': associate_id,
        'associate_name': associate_name,
        'category': category,
        'associates': associates,
        'learning_skills': learning_skills,
        'cdps_awarded': cdps_awarded,
        'learning_pendingcount': learning_pendingcount,
        'sharing_pendingcount': sharing_pendingcount,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'alert': alert
    }
    return render(request, 'manager/manager_learning.html', context)

@login_required
def manager_learning_update(request, id):
    learning_skill = LearningSkill.objects.get(id=id)
    learning_skill.status = request.POST['status']
    learning_skill.cdp_awarded = request.POST['cdp_awarded']
    learning_skill.last_updated_by = str(request.user)
    learning_skill.save()
    if request.POST['status'] == 'Close':
        messages.info(request, 'approved')
    else:
        messages.info(request, 'updated')
    if request.POST['page'] == "pending_approvals":
        return redirect('/pending_approvals/' + 'learning')
    else:
        return redirect('/manager_learning/' + learning_skill.associate_id + '/' + ' ')

#-----------------------------------------------------------------------------------------------#
# MANAGER - Sharing Skills
#-----------------------------------------------------------------------------------------------#

@login_required
def manager_sharing(request, associate_id, selected_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, associate_id, selected_cycle, 'financial')
    alert = get_message(request)
    try:
        status = ['Pending Approval','Close']
        sharing_skills = SharingSkill.objects.filter(associate_id = associate_id, status__in = status, created_date__range=[startdate, enddate]).extra(
                        select={'associate_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = SharingSkill.associate_id',
                                'project_id':
                                'Select project_id from Associate where '
                                'Associate.associate_id = SharingSkill.associate_id',
                                'project_name':
                                'Select project_name from Associate where '
                                'Associate.associate_id = SharingSkill.associate_id'}
                                )
    except:
        cdps_awarded = {}
        sharing_skills = {} 
        startdate = 0
        enddate = 0   

    cdps_awarded = ['1', '2', '3', '4', '5']
    category = 'Sharing Skills'

    # Get the Competence Pending Counts
    learning_pendingcount, sharing_pendingcount = get_pending_competence(associate_id, startdate, enddate)

    context = {
        'login' : login,
        'associate_id': associate_id,
        'associate_name': associate_name,
        'category': category,
        'associates': associates,
        'sharing_skills': sharing_skills,
        'cdps_awarded': cdps_awarded,
        'learning_pendingcount': learning_pendingcount,
        'sharing_pendingcount': sharing_pendingcount,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'alert': alert
    }
    return render(request, 'manager/manager_sharing.html', context)

@login_required
def manager_sharing_update(request, id):
    sharing_skill = SharingSkill.objects.get(id=id)
    sharing_skill.status = request.POST['status']
    sharing_skill.cdp_awarded = request.POST['cdp_awarded']
    sharing_skill.last_updated_by = str(request.user)
    sharing_skill.save()
    if request.POST['status'] == 'Close':
        messages.info(request, 'approved')
    else:
        messages.info(request, 'updated')
    if request.POST['page'] == "pending_approvals":
        return redirect('/pending_approvals/' + 'sharing')
    else:
        return redirect('/manager_sharing/' + sharing_skill.associate_id + '/' + ' ')

#-----------------------------------------------------------------------------------------------#
# MANAGER - Technical & Managerial Skills
#-----------------------------------------------------------------------------------------------#

@login_required
def manager_technical_managerial(request, associate_id, selected_cycle):
    context = manager_competence_transactions(request, associate_id, selected_cycle, 'Competence')
    return render(request, 'manager/manager_technical_managerial.html', context)

@login_required
def manager_technical_managerial_update(request, associate_id, effective_end):
    manager_competence_update(request, associate_id, effective_end, 'Competence')
    return redirect('/manager_technical_managerial/' + associate_id + '/' + ' ') 

#-----------------------------------------------------------------------------------------------#
# MANAGER - Workplace Behaviour
#-----------------------------------------------------------------------------------------------#

@login_required
def manager_workplace_behaviour(request, associate_id, selected_cycle):
    context = manager_competence_transactions(request, associate_id, selected_cycle, 'Organization Value')
    return render(request, 'manager/manager_workplace_behaviour.html', context)

@login_required
def manager_workplace_behaviour_update(request, associate_id, effective_end):
    manager_competence_update(request, associate_id, effective_end, 'Organization Value')
    return redirect('/manager_workplace_behaviour/' + associate_id + '/' + ' ')

#-----------------------------------------------------------------------------------------------#
# MANAGER - Organization Development
#-----------------------------------------------------------------------------------------------#

@login_required
def manager_organization_development(request, associate_id, selected_cycle):
    context = manager_competence_transactions(request, associate_id, selected_cycle, 'Organization Development')
    return render(request, 'manager/manager_organization_development.html', context)

@login_required
def manager_organization_development_update(request, associate_id, effective_end):
    manager_competence_update(request, associate_id, effective_end, 'Organization Development')
    return redirect('/manager_organization_development/' + associate_id + '/' + ' ')
    
#-----------------------------------------------------------------------------------------------------#
# ASSOCIATES' COMPETENCE - Common for Technical & Managerial, Workplace Behaviour and Org Development
#-----------------------------------------------------------------------------------------------------#

def manager_competence_transactions(request, associate_id, selected_cycle, category_type):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, associate_id, selected_cycle, 'financial')
    alert = get_message(request)
    try:
        categories = CompetenceTransaction.objects.filter(associate_id = associate_id, category_type = category_type, created_date__range=[startdate, enddate]).order_by('category').extra(
                    select = {'associate_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = CompetenceTransaction.associate_id ',
                                'project_id':
                                'Select project_id from Associate where '
                                'Associate.associate_id = CompetenceTransaction.associate_id',
                                'project_name':
                                'Select project_name from Associate where '
                                'Associate.associate_id = CompetenceTransaction.associate_id '})
    except:
        categories = {}
        startdate = 0
        enddate = 0
    
    # Get the Average Rating from Ratings table. If not found, Calculate the average
    try:
        avg_rating = get_rating(associate_id, selected_cycle, rating_type)
        if not avg_rating:
            total_rating = 0
            count = 0
            for category in categories:
                total_rating = total_rating + category.manager_rating
                count = count + 1
            avg_rating = total_rating/count
        avg_rating = round(avg_rating, 2)
        avg_rating = round(avg_rating*4)/4
    except:
        avg_rating = 0
    
    if category_type == 'Competence':
        rating_type = 'rating_technical_managerial'
        category = 'Technical & Managerial'
    elif category_type == 'Organization Value':
        rating_type = 'rating_workplace_behaviour'
        category = 'Workplace Behaviour'
    elif category_type == 'Organization Development':
        rating_type = 'rating_org_development'
        category = 'Organization Development'

    context = {
        'login' : login,
        'associate_id': associate_id,
        'associate_name': associate_name,
        'category': category,
        'associates': associates,
        'categories': categories,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'avg_rating': avg_rating,
        'alert': alert
        }
    return context

@login_required
def manager_competence_update(request, associate_id, effective_end, category_type):
    skills = CompetenceTransaction.objects.filter(associate_id = associate_id, effective_end = effective_end, category_type = category_type).order_by('category')
    for updateskill in skills:
        updateskill = CompetenceTransaction.objects.get(associate_id = associate_id, effective_end = effective_end, category = updateskill.category)
        updateskill.manager_rating = request.POST[updateskill.category + '_' + "manager_rating"]
        updateskill.manager_feedback = request.POST[updateskill.category + '_' + "manager_feedback"]
        updateskill.status = request.POST['status']
        updateskill.last_updated_by = str(request.user)
        updateskill.save()
    if request.POST['status'] == 'Close':
        messages.info(request, 'approved')
    else:
        messages.info(request, 'updated')
    cycle = request.POST['selected_cycle']
        
    if category_type == 'Competence':
        rating_type = 'rating_technical_managerial'
    elif category_type == 'Organization Value':
        rating_type = 'rating_workplace_behaviour'
    elif category_type == 'Organization Development':
        rating_type = 'rating_org_development'

    set_rating(request, associate_id, cycle, rating_type)
    return

#-----------------------------------------------------------------------------------------------#
# MANAGER - Associates' Key Result Areas
#-----------------------------------------------------------------------------------------------#

@login_required
def copy_kras(request):
    associates = Associate.objects.filter(manager_id = request.user).order_by('associate_name')
    for associate in associates:
        mastercategories = MasterCategory.objects.all()
        for mastercategory in mastercategories:
            kracategorytransaction = KraCategoryTransaction(
                associate_id = associate.associate_id,
                associate_name = associate.associate_name,
                category = mastercategory.category,
                weightage = mastercategory.weightage,
                category_target = mastercategory.target,
                associate_comments = ' ',
                manager_feedback = ' ',
                manager_rating = 0,
                status = 'Create',
                effective_start = request.POST['selected_start'],
                effective_end = request.POST['selected_end'],
                created_by = request.user)
            kracategorytransaction.save()

            mastersubcategories = MasterSubCategory.objects.filter(category = mastercategory.category)
            for mastersubcategory in mastersubcategories:
                krasubcategorytransaction = KraSubCategoryTransaction(
                    associate_id = associate.associate_id,
                    associate_name = associate.associate_name,
                    category = mastersubcategory.category,
                    subcategory = mastersubcategory.subcategory,
                    subcategory_target = mastercategory.target,
                    associate_comments = ' ',
                    manager_feedback = ' ',
                    manager_rating = 0,
                    status = 'Create',
                    effective_start = request.POST['selected_start'],
                    effective_end = request.POST['selected_end'],
                    created_by = request.user)
                krasubcategorytransaction.save()
    return redirect('/manager_kras/' + associate_id + '/' + ' ' + '/' + ' ')

# Key Result Areas
@login_required
def manager_kras(request, associate_id, selected_cycle, performance_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, associate_id, selected_cycle, 'financial')
    alert = get_message(request)

    # To decide which ratings to be displayed
    cycle, performance_cycle, startdate, enddate, effective_end = get_misc_data(selected_cycle, performance_cycle)
    try:
        categories = KraCategoryTransaction.objects.filter(associate_id = associate_id, effective_end = effective_end).order_by('category').extra(
                    select = {'project_id':
                                'Select project_id from Associate where '
                                'Associate.associate_id = KraCategoryTransaction.associate_id',
                              'project_name':
                                'Select project_name from Associate where '
                                'Associate.associate_id = KraCategoryTransaction.associate_id '})
        if categories:
            # for category in categories:
                # subcategories = KraSubCategoryTransaction.objects.filter(associate_id = associate_id, effective_end = effective_end).order_by('subcategory')          
            
            # To decide the current KRA Cycle 
            if performance_cycle == 'cycle1' :
                rating_type = 'rating_kra_cycle1'
            elif performance_cycle == 'cycle2' :
                rating_type = 'rating_kra_cycle2'
            elif performance_cycle == 'cycle3' :
                rating_type = 'rating_kra_cycle3'
            avg_rating = get_rating(request.user, selected_cycle, rating_type)

            if avg_rating == None:
                total_rating = 0
                count = 0
                for category in categories:
                    total_rating = total_rating + category.manager_rating
                    count = count + 1
                avg_rating = total_rating/count
                avg_rating = round(avg_rating, 2)
                avg_rating = round(avg_rating*4)/4 
        else:
            categories = {}
            # subcategories = {}
            avg_rating = {}
             
    except:
        categories = {}
        # subcategories = {}
        avg_rating = {}
        startdate = 0
        enddate = 0

    # Get the Workperformance Pending Counts
    awards_pendingcount, appreciations_pendingcount, feedback_pendingcount = get_pending_work_performance(associate_id, startdate, enddate)
    kra_category = 'Kras'
    
    context = {
        'login' : login,
        'categories': categories,
        'associate_id': associate_id,
        'kra_category': kra_category,
        'associates': associates,
        # 'subcategories': subcategories,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'performance_cycle': performance_cycle,
        'cycle': cycle,
        'avg_rating': avg_rating,
        'awards_pendingcount': awards_pendingcount,
        'appreciations_pendingcount': appreciations_pendingcount,
        'feedback_pendingcount': feedback_pendingcount,
        'alert': alert,
        }
    return render(request, 'manager/manager_kras.html', context)

@login_required
def manager_kras_update(request, associate_id, effective_end):
    kracategories = KraCategoryTransaction.objects.filter(associate_id = associate_id, effective_end = effective_end)
    for kracategory in kracategories:
        updatekracategory = KraCategoryTransaction.objects.get(associate_id = associate_id, category = kracategory.category, effective_end = effective_end)
        if request.POST['status'] == 'Create' or request.POST['status'] == 'Open':
            updatekracategory.weightage = request.POST[kracategory.category + '_' + "weightage"]
            updatekracategory.target = request.POST[kracategory.category + '_' + "target"]
        else:
            updatekracategory.manager_feedback = request.POST[kracategory.category + '_' + "manager_feedback"]
            updatekracategory.manager_rating = request.POST[kracategory.category + '_' + "manager_rating"]
        updatekracategory.status = request.POST['status']
        updatekracategory.last_updated_by = str(request.user)
        updatekracategory.save()

        # krasubcategories = KraSubCategoryTransaction.objects.filter(associate_id = associate_id, category = kracategory.category, effective_end = effective_end)
        # for krasubcategory in krasubcategories:
        #     updatesubkracategory = KraSubCategoryTransaction.objects.get(associate_id = associate_id, subcategory = krasubcategory.subcategory, effective_end = effective_end)
        #     updatesubkracategory.subcategory_target = request.POST[krasubcategory.subcategory + '_' + "target"]
        #     updatesubkracategory.manager_feedback = request.POST[krasubcategory.subcategory + '_' + "manager_feedback"]
        #     updatesubkracategory.manager_rating = request.POST[krasubcategory.subcategory + '_' + "manager_rating"]
        #     updatesubkracategory.status = request.POST['status']
        #     updatesubkracategory.last_updated_by = str(request.user)
        #     updatesubkracategory.save()
    
    performance_cycle = request.POST['performance_cycle']
    selected_cycle = request.POST['selected_cycle']
    if request.POST['status'] == 'Pending Approval' or request.POST['status'] == 'Close':
        
        # To decide the current KRA Cycle 
        if performance_cycle == 'cycle1' :
            rating_type = 'rating_kra_cycle1'
        elif performance_cycle == 'cycle2' :
            rating_type = 'rating_kra_cycle2'
        elif performance_cycle == 'cycle3' :
            rating_type = 'rating_kra_cycle3'
        set_rating(request, associate_id, selected_cycle, rating_type)

    if request.POST['status'] == 'Close':
        messages.info(request, 'approved')
    else:
        messages.info(request, 'updated')
    return redirect('/manager_kras/' + associate_id + '/' + selected_cycle + '/' + performance_cycle)

#-----------------------------------------------------------------------------------------------#
# MANAGER - Associates' Work Performance 
#-----------------------------------------------------------------------------------------------#

# Awards
@login_required
def manager_awards(request, associate_id, selected_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, associate_id, selected_cycle, 'financial')
    alert = get_message(request)    
    try:
        status = ['Pending Approval','Approved']
        awards = Award.objects.filter(associate_id = associate_id, status__in = status, created_date__range=[startdate, enddate]).extra(
                        select={'associate_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Award.associate_id',
                                'project_id':
                                'Select project_id from Associate where '
                                'Associate.associate_id = Award.associate_id',
                                'project_name':
                                'Select project_name from Associate where '
                                'Associate.associate_id = Award.associate_id'}
                                )
    except:
        awards = {}
        startdate = 0
        enddate = 0
    # Get the Workperformance Pending Counts
    awards_pendingcount, appreciations_pendingcount, feedback_pendingcount = get_pending_work_performance(associate_id, startdate, enddate)
    kra_category = 'Awards'

    context = {
        'login' : login,
        'awards': awards,
        'associate_id': associate_id,
        'kra_category': kra_category,
        'associates': associates,
        'awards_pendingcount': awards_pendingcount,
        'appreciations_pendingcount': appreciations_pendingcount,
        'feedback_pendingcount': feedback_pendingcount,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'alert': alert
    }
    return render(request, 'manager/manager_awards.html', context)

# Appreciations
@login_required
def manager_appreciations(request, associate_id, selected_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, associate_id, selected_cycle, 'financial')
    alert = get_message(request)
    try:
        status = ['Pending Approval','Approved']
        commitments = Commitment.objects.filter(associate_id = associate_id, status__in = status, created_date__range=[startdate, enddate]).extra(
                        select={'associate_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Commitment.associate_id',
                                'project_id':
                                'Select project_id from Associate where '
                                'Associate.associate_id = Commitment.associate_id',
                                'project_name':
                                'Select project_name from Associate where '
                                'Associate.associate_id = Commitment.associate_id',
                                'recognised_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Commitment.recognised_by'}
                                )
    except:
        commitments = {}
        startdate = 0
        enddate = 0
    
    # Get the Workperformance Pending Counts
    awards_pendingcount, appreciations_pendingcount, feedback_pendingcount = get_pending_work_performance(associate_id, startdate, enddate)
    kra_category = 'Appreciations'

    context = {
        'login' : login,
        'commitments': commitments,
        'associate_id': associate_id,
        'kra_category': kra_category,
        'associates': associates,
        'awards_pendingcount': awards_pendingcount,
        'appreciations_pendingcount': appreciations_pendingcount,
        'feedback_pendingcount': feedback_pendingcount,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'alert': alert
    }
    return render(request, 'manager/manager_appreciations.html', context)

# Improvement Areas or Feedback
@login_required
def manager_feedback(request, associate_id, selected_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, associate_id, selected_cycle, 'financial')
    alert = get_message(request)
    try:
        status = ['In Progress', 'Action Approval', 'Pending Approval', 'Close']
        feedback = Feedback.objects.filter(associate_id = associate_id, status__in = status, created_date__range=[startdate, enddate]).extra(
                       select={'associate_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Feedback.associate_id',
                                'project_id':
                                'Select project_id from Associate where '
                                'Associate.associate_id = Feedback.associate_id',
                                'project_name':
                                'Select project_name from Associate where '
                                'Associate.associate_id = Feedback.associate_id',
                                'feedback_by_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Feedback.feedback_by'}
                                )
    except:
        feedback = {}
        startdate = 0
        enddate = 0
    
    # Get the Workperformance Pending Counts
    awards_pendingcount, appreciations_pendingcount, feedback_pendingcount = get_pending_work_performance(associate_id, startdate, enddate)
    kra_category = 'Feedbacks'

    context = {
        'login' : login,
        'feedback': feedback,
        'associate_id': associate_id,
        'kra_category': kra_category,
        'associates': associates,
        'awards_pendingcount': awards_pendingcount,
        'appreciations_pendingcount': appreciations_pendingcount,
        'feedback_pendingcount': feedback_pendingcount,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'alert': alert
    }
    return render(request, 'manager/manager_feedback.html', context)

#-----------------------------------------------------------------------------------------------#
# MANAGER: Pending Approvals
#-----------------------------------------------------------------------------------------------#
@login_required
def pending_approvals(request, selected_type):
    alert = get_message(request)
    login = Associate.objects.get(associate_id = request.user)
    associates = Associate.objects.filter(manager_id = request.user).values_list('associate_id')
    learning_skills = LearningSkill.objects.filter(status = "Pending Approval", associate_id__in = associates) \
                                            .extra(select={
                                            'associate_name':
                                            'Select associate_name from Associate where '
                                            'Associate.associate_id = LearningSkill.associate_id',
                                            'project_name':
                                            'Select project_name from Associate where '
                                            'Associate.associate_id = LearningSkill.associate_id'}
                                            )
    sharing_skills = SharingSkill.objects.filter(status = "Pending Approval", associate_id__in = associates) \
                                            .extra(select={
                                            'associate_name':
                                            'Select associate_name from Associate where '
                                            'Associate.associate_id = SharingSkill.associate_id',
                                            'project_name':
                                            'Select project_name from Associate where '
                                            'Associate.associate_id = SharingSkill.associate_id'}
                                            ) 
    awards = Award.objects.filter(status = "Pending Approval", associate_id__in = associates) \
                                            .extra(select={
                                            'associate_name':
                                            'Select associate_name from Associate where '
                                            'Associate.associate_id = Award.associate_id',
                                            'project_name':
                                            'Select project_name from Associate where '
                                            'Associate.associate_id = Award.associate_id'}
                                            )
    appreciations = Commitment.objects.filter(status = "Pending Approval", associate_id__in = associates) \
                                            .extra(select={
                                            'associate_name':
                                            'Select associate_name from Associate where '
                                            'Associate.associate_id = Commitment.associate_id',
                                            'project_name':
                                            'Select project_name from Associate where '
                                            'Associate.associate_id = Commitment.associate_id',
                                            'recognised_name':
                                            'Select associate_name from Associate where '
                                            'Associate.associate_id = Commitment.recognised_by'}
                                            )
    status = ["Pending Approval","Action Approval"]
    feedbacks = Feedback.objects.filter(status__in = status, associate_id__in = associates) \
                                            .extra(
                                            select={'associate_name':
                                            'Select associate_name from Associate where '
                                            'Associate.associate_id = Feedback.associate_id',
                                            'project_name':
                                            'Select project_name from Associate where '
                                            'Associate.associate_id = Feedback.associate_id',
                                            'feedback_by_name':
                                            'Select associate_name from Associate where '
                                            'Associate.associate_id = Feedback.feedback_by'}
                                            )
    cdps_awarded = ['1', '2', '3', '4', '5']
    context = {
        'login' : login,
        'learning_skills': learning_skills,
        'sharing_skills': sharing_skills,
        'awards': awards,
        'appreciations': appreciations,
        'feedbacks': feedbacks,
        'cdps_awarded': cdps_awarded,
        'selected_type': selected_type,
        'alert': alert
        }
    return render(request, 'manager/pending_approvals.html', context)

#-----------------------------------------------------------------------------------------------#
# MANAGER: Extract Data from JIRA 
#-----------------------------------------------------------------------------------------------#

@login_required
def jira(request, jira_id):
    error = get_message(request)
    currentMonth = datetime.now().month
    currentYear = datetime.now().year
    totalmonths = currentYear * 12 + currentMonth - 1 # Months since year 0 minus 1
    last12months = []
    for i in range(12):
        month = calendar.month_abbr[(totalmonths - i) % 12 + 1] + ', ' + str((totalmonths - i) // 12)
        last12months.append(month)
    login = Associate.objects.get(associate_id = request.user)
    competencies = Jira.objects.order_by('-year', '-month', 'associate_id') \
                                .extra(select=
                                    {'associate_name':
                                        'Select associate_name from Associate where '
                                        'Associate.associate_id = Jira.associate_id'}
                                )

    projects = Project.objects.all()
    context = {
        'login' : login,
        'projects': projects,
        'jira_id': jira_id,
        'last12months': last12months,
        'competencies' : competencies,
        'error': error
        }
    return render(request, 'manager/jira.html', context)
    
@login_required
def get_Data_From_Jira(request):
    error = ''
    # Connect to JIRA
    options = {'server': 'https://senecaglobal.jira.com/'}
    try:
        jira = JIRA(options, basic_auth=(request.POST['email_address'], request.POST['password']), max_retries = 0)
        selectedProjectKey = request.POST['selectedProjectKey']
        selectedMonth = request.POST['selectedMonth']
        FromDate, ToDate = get_selected_date_range(selectedMonth)
        QUERY = 'project = ' + selectedProjectKey + ' AND (resolution = "FIXED" OR resolution = "DONE") AND resolutiondate >= "' + FromDate + '" AND resolutiondate <= "' + ToDate + '" ORDER BY assignee'
        # QUERY = 'project = ' + selectedProjectKey + ' AND (resolution = "FIXED" OR resolution = "DONE") AND resolutiondate >= "2018-03-01 00:00" AND resolutiondate <= "2018-06-01 00:00" ORDER BY assignee'
        try:
            issues = jira.search_issues(QUERY, maxResults= 100)
            # Update Jira Table
            update_jira(issues, selectedProjectKey)

        except JIRAError as e:
            if e.status_code == 400:
                error = "Project does not exist or You are unauthorized. Please contact admin"
                messages.info(request, error)
    except JIRAError as e:
        if e.status_code == 401:
            error = "Username or Password incorrect. Login to JIRA failed. Please try with valid credentials."
            messages.info(request, error)
    return redirect('/jira/' + ' ')

def update_jira(issues, selectedProjectKey):
    jiraDict = {}
    i = error = 0
    task, competencies, developer, stories, totaltasks, complianttasks, complianttaskdetails, \
    compdata, defects, avg_ratings, rating_counts, jiraList = ([] for i in range(12))

    for issue in issues:
        count = 0
        try:
            task_developer = issue.fields.customfield_11561.name
            if task_developer == ' ':
                task_developer = issue.fields.assignee.name
        except:
            task_developer = issue.fields.assignee.name
        developer.append(task_developer) 
        totaltasks.append(1) 
        story_point = issue.fields.customfield_10033
        eff_Coding = issue.fields.customfield_13407
        eff_Impact_Analysis = issue.fields.customfield_13401
        eff_Req_Analysis = issue.fields.customfield_13400
        eff_Test_Cases = issue.fields.customfield_13403
        meeting_Req = issue.fields.customfield_13405
        multi_Solution = issue.fields.customfield_13402
        coding_Standard = issue.fields.customfield_13406
        testing_Abilities = issue.fields.customfield_13408

        # Stories
        try:
            stories = (round(issue.fields.customfield_10033,2))
        except:
            stories = 0

        # Requirement Analysis, Coding and Testing - Ratings
        rating_count = 0
        try:
            effCoding = int(eff_Coding.value)
            rating_count += 1
        except: 
            effCoding = 0
        try:
            effImpactAnalysis = int(eff_Impact_Analysis.value)
            rating_count += 1
        except:
            effImpactAnalysis = 0
        try:
            effReqAnalysis = int(eff_Req_Analysis.value)
            rating_count += 1
        except:
            effReqAnalysis = 0
        try:
            effTestCases = int(eff_Test_Cases.value)
            rating_count += 1
        except: 
            effTestCases = 0
        try:
            meetingReq = int(meeting_Req.value)
            rating_count += 1
        except:
            meetingReq = 0
        try:
            multiSolution = int(multi_Solution.value)
            rating_count += 1
        except:
            multiSolution = 0
        try:
            codingStandard = int(coding_Standard.value)
            rating_count += 1
        except: 
            codingStandard = 0
        try:
            testingAbilities = int(testing_Abilities.value)
            rating_count += 1
        except:
            testingAbilities = 0   

        if rating_count > 0:
            avg_rating = (effCoding + effImpactAnalysis + effReqAnalysis + effTestCases + meetingReq + codingStandard + multiSolution + testingAbilities) / rating_count
            avg_ratings.append(avg_rating)
            rating_counts.append(rating_count)
            count += 1
        else:
            avg_rating = 0
            rating_count = 0
        avg_ratings.append(avg_rating)
        rating_counts.append(rating_count)

        # Due Date and Closed Date   
        try:
            duedate = issue.fields.duedate
            if duedate is None:
                duedate = issue.fields.resolutiondate[0:10]
        except:
            duedate = issue.fields.resolutiondate[0:10]                
        closedate = issue.fields.resolutiondate[0:10]

        # Get the Compliant tasks details
        if duedate < closedate:
            complianttask = 1
        else:
            complianttask = 0
            # compdata.append(issue.fields.assignee.name)
            # compdata.append(str(issue))
            # compdata.append(issue.fields.duedate)
            # compdata.append(issue.fields.resolutiondate[0:10])
            # complianttaskdetails.append(compdata)                            
        complianttasks.append(complianttask)

        # Count the number of Defects
        issuetype = str(issue.fields.issuetype)
        if (issuetype.find('Defect') != -1): 
            defect = 1
        else:
            defect = 0
        defects.append(defect)
        jiraDict = {
                'developer': issue.fields.assignee.name + ' ' + issue.fields.resolutiondate[0:7],
                'stories': stories,
                'total_tasks': 1,
                'compliant_task': complianttask,
                'defect': defect,
                'avg_rating': avg_rating,
                'count': count
                }
        jiraList.append(jiraDict)

    index_key = 'developer'
    output_dict = {}
    jiraList1 = []
    for d in jiraList:
        index = d[index_key]
        if index not in output_dict:
            output_dict[index] = {}
        for k, v in d.items():
            if k not in output_dict[index]:
                output_dict[index][k] = v
            elif k != index_key:
                output_dict[index][k] += v
    jiraList1 = output_dict.values()
    
    for rec in jiraList1:
        # Update the record if already exists or add a new one
        developer = rec['developer']
        Month = developer[-2:]
        Year  = developer[-7:]
        developer = developer[0:developer.find(' ')]
        Year  = Year[0:4]
        try:
            jira = Jira.objects.get(jira_id = rec['developer'], month = Month, year = Year)
            jira.project_id = selectedProjectKey
            jira.month = Month
            jira.year = Year
            jira.jira_id = developer
            try:
                associate = Associate.objects.get(jira_id = jira.jira_id)
                jira.associate_id = associate.associate_id
            except:
                jira.associate_id = 'Some'
            jira.story_points = rec['stories']
            jira.total_tasks = rec['total_tasks']
            jira.due_compliant_tasks = rec['compliant_task']
            jira.ontime_delivery = (rec['total_tasks'] - rec['compliant_task']) * 100 / rec['total_tasks']
            jira.defects = rec['defect']
            jira.firsttime_correct = (rec['total_tasks'] - rec['defect']) * 100 / rec['total_tasks']
            if rec['count'] > 0:
                jira.technical_competence_rating = rec['avg_rating'] / rec['count']
            else:
                jira.technical_competence_rating = 0
            jira.save()

        except:
            jira = Jira()
            jira.project_id = selectedProjectKey
            jira.month = Month
            jira.year = Year
            jira.jira_id = developer
            try:
                associate = Associate.objects.get(jira_id = jira.jira_id)
                jira.associate_id = associate.associate_id
            except:
                jira.associate_id = 'Some'
            jira.story_points = rec['stories']
            jira.total_tasks = rec['total_tasks']
            jira.due_compliant_tasks = rec['compliant_task']
            jira.ontime_delivery = (rec['total_tasks'] - rec['compliant_task']) * 100 / rec['total_tasks']
            jira.defects = rec['defect']
            jira.firsttime_correct = (rec['total_tasks'] - rec['defect']) * 100 / rec['total_tasks']
            if rec['count'] > 0:
                jira.technical_competence_rating = rec['avg_rating'] / rec['count']
            else:
                jira.technical_competence_rating = 0
            jira.save()
    return None
	
#-----------------------------------------------------------------------------------------------#
# MANAGER - Final Rating
#-----------------------------------------------------------------------------------------------#

@login_required
def final_rating(request, selected_cycle, performance_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, financial_cycles = get_details(request, ' ', selected_cycle, 'financial')
    alert = get_message(request)

    # To decide which ratings to be displayed
    cycle, performance_cycle, startdate, enddate, effective_end = get_misc_data(selected_cycle, performance_cycle)

    # Loop for all Associates
    ratingList = []
    ratingDict = {}
    for associate in associates:
        pending_flag = ' '
        # COUNTS
        learning_count = LearningSkill.objects.filter(associate_id = associate.associate_id, status = 'Close', created_date__range=[startdate, enddate]).count()
        learning_pending = LearningSkill.objects.filter(associate_id = associate.associate_id, status = 'Pending Approval', created_date__range=[startdate, enddate]).count()

        sharing_count = SharingSkill.objects.filter(associate_id = associate.associate_id, status = 'Close', created_date__range=[startdate, enddate]).count()
        sharing_pending = SharingSkill.objects.filter(associate_id = associate.associate_id, status = 'Pending Approval', created_date__range=[startdate, enddate]).count()

        awards_count = Award.objects.filter(associate_id = associate.associate_id, created_date__range=[startdate, enddate], status = 'Approved').count()
        awards_pending = Award.objects.filter(associate_id = associate.associate_id, created_date__range=[startdate, enddate], status = 'Pending Approval').count()
        
        appreciations_count = Commitment.objects.filter(associate_id = associate.associate_id, created_date__range=[startdate, enddate], status = 'Approved').count()
        appreciations_pending = Commitment.objects.filter(associate_id = associate.associate_id, created_date__range=[startdate, enddate], status = 'Pending Approval').count()

        status = ['In Progress', 'Action Approval', 'Close']
        feedbacks_count = Feedback.objects.filter(associate_id = associate.associate_id, created_date__range=[startdate, enddate], status__in = status).count()
        feedbacks_pending = Feedback.objects.filter(associate_id = associate.associate_id, created_date__range=[startdate, enddate], status = 'Pending Approval').count()

        # RATINGS
        try:
            rec = Ratings.objects.get(associate_id = associate.associate_id, cycle = selected_cycle)
            technical_managerial = rec.rating_technical_managerial
            workplace_behaviour = rec.rating_workplace_behaviour
            org_development = rec.rating_org_development

            if performance_cycle == 'cycle1' or performance_cycle == 'cycle2' or performance_cycle == 'cycle3':
                kra_rating_cycle1, kra_rating_cycle2, kra_rating_cycle3, rating_cycle1, rating_cycle2, rating_cycle3 = (' ' for i in range(6))

            if performance_cycle == 'cycle1':
                kra_rating = rec.rating_kra_cycle1
                overall_rating = rec.rating_cycle1
                remarks = rec.remarks_cycle1

            elif performance_cycle == 'cycle2':
                kra_rating = rec.rating_kra_cycle2
                overall_rating = rec.rating_cycle2
                remarks = rec.remarks_cycle2

            elif performance_cycle == 'cycle3':   
                kra_rating = rec.rating_kra_cycle3
                overall_rating = rec.rating_cycle3
                remarks = rec.remarks_cycle3

            elif performance_cycle == 'annual': 
                kra_rating_cycle1 = rec.rating_kra_cycle1 
                kra_rating_cycle2 = rec.rating_kra_cycle2 
                kra_rating_cycle3 = rec.rating_kra_cycle3
                rating_cycle1 = rec.rating_cycle1
                rating_cycle2 = rec.rating_cycle2
                rating_cycle3 = rec.rating_cycle3   
                kra_rating = ' '
                overall_rating = rec.rating_annual
                remarks = rec.remarks_annual

        except:
            technical_managerial, workplace_behaviour, org_development, kra_rating, overall_rating, \
            kra_rating_cycle1, kra_rating_cycle2, kra_rating_cycle3, \
            rating_cycle1, rating_cycle2, rating_cycle3, remarks = (' ' for i in range(12))
            pending_flag = 'Y'

        if learning_pending > 0 or sharing_pending > 0 or awards_pending > 0 or appreciations_pending > 0 or feedbacks_pending > 0 or kra_rating == None:
            pending_flag = 'Y'
        
        ratingDict = {
                'associate_name': associate.associate_name,
                'associate_id': associate.associate_id,
                'project_name': associate.project_name,
                'learning_count': learning_count,
                'learning_pending': learning_pending,
                'sharing_count': sharing_count,
                'sharing_pending': sharing_pending,
                'technical_managerial': technical_managerial,
                'workplace_behaviour': workplace_behaviour,
                'org_development': org_development,
                'kra_rating_cycle1': kra_rating_cycle1, 
                'kra_rating_cycle2': kra_rating_cycle2,
                'kra_rating_cycle3': kra_rating_cycle3,
                'rating_cycle1': rating_cycle1,
                'rating_cycle2': rating_cycle3,
                'rating_cycle3': rating_cycle3,
                'remarks': remarks,
                'kra_rating': kra_rating,
                'overall_rating': overall_rating,
                'awards_count': awards_count,
                'awards_pending': awards_pending, 
                'appreciations_count': appreciations_count,
                'appreciations_pending': appreciations_pending,
                'feedbacks_count': feedbacks_count,
                'feedbacks_pending': feedbacks_pending, 
                'pending_flag': 'pending_flag',
                }
        ratingList.append(ratingDict)

    context = {
        'login' : login,
        'ratingList': ratingList,
        'financial_cycles': financial_cycles,
        'selected_cycle': selected_cycle,
        'performance_cycle': performance_cycle,
        'cycle': cycle,
        'alert': alert,
    }
    return render(request, 'manager/final_rating.html', context)

@login_required
def final_rating_update(request, associate_id):
    selected_cycle = request.POST['selected_cycle']
    performance_cycle = request.POST['performance_cycle']
    rec = Ratings.objects.get(associate_id = associate_id, cycle = selected_cycle)
    if performance_cycle == 'cycle1':
        rec.rating_cycle1 = request.POST['overall_rating']
        rec.remarks_cycle1 = request.POST['remarks']
    elif performance_cycle == 'cycle2':
        rec.rating_cycle2 = request.POST['overall_rating']
        rec.remarks_cycle2 = request.POST['remarks']
    elif performance_cycle == 'cycle3':
        rec.rating_cycle3 = request.POST['overall_rating']
        rec.remarks_cycle3 = request.POST['remarks']
    elif performance_cycle == 'annual':
        rec.rating_annual = request.POST['overall_rating']
        rec.remarks_annual = request.POST['remarks']
    rec.save()
    messages.info(request, 'updated')    
    return redirect('/final_rating/' + selected_cycle + '/' + performance_cycle)

#-----------------------------------------------------------------------------------------------#
# COMPETENCE - Learning Skills
#-----------------------------------------------------------------------------------------------#

@login_required
def learning(request, selected_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, ' ', selected_cycle, 'financial')
    alert = get_message(request)
    try:
        learning_skills = LearningSkill.objects.filter(associate_id = request.user, created_date__range=[startdate, enddate])
        opencount = LearningSkill.objects.filter(associate_id = request.user, status = 'Open', created_date__range=[startdate, enddate]).count()
    except:
        learning_skills = {}
        opencount = 0

    skills_applied_flags = ['Yes', 'No']
    proficiency_levels = ['Basic', 'Beginner']

    context = {
        'login' : login,
        'learning_skills': learning_skills,
        'skills_applied_flags': skills_applied_flags,
        'proficiency_levels': proficiency_levels,
        'opencount': opencount,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'alert': alert
    }
    return render(request, 'competence/learning.html', context)

@login_required
def learning_add(request):
    learning_skill = LearningSkill()
    learning_skill.associate_id = request.user
    learning_skill.skill_title = request.POST['skill_title']
    learning_skill.course_title = request.POST['course_title']
    learning_skill.skill_duration_hours = request.POST['skill_duration_hours']
    learning_skill.outcome_deliverable = request.POST['outcome_deliverable']
    learning_skill.certification = request.POST['certification']
    learning_skill.skills_applied_flag = request.POST['skills_applied_flag']
    learning_skill.proficiency_level = request.POST['proficiency_level']
    learning_skill.status = request.POST['status_add']
    learning_skill.created_date = request.POST['created_date']
    learning_skill.created_by = request.user
    learning_skill.save()
    messages.info(request, 'added')
    learning_skills = LearningSkill.objects.all()
    context = {'learning_skills': learning_skills}
    return redirect('/learning/' + ' ')

@login_required
def learning_update(request, id):
    learning_skill = LearningSkill.objects.get(id=id)
    learning_skill.skill_title = request.POST['skill_title']
    learning_skill.course_title = request.POST['course_title']
    learning_skill.skill_duration_hours = request.POST['skill_duration_hours']
    learning_skill.outcome_deliverable = request.POST['outcome_deliverable']
    learning_skill.certification = request.POST['certification']
    learning_skill.skills_applied_flag = request.POST['skills_applied_flag']
    learning_skill.proficiency_level = request.POST['proficiency_level']
    learning_skill.status = request.POST['status_edit']
    learning_skill.created_date = request.POST['created_date']
    learning_skill.last_updated_by = str(request.user)
    learning_skill.save()
    if request.POST['status_edit'] == 'Open':
        messages.info(request, 'updated')
    else:
        messages.info(request, 'submitted')
    return redirect('/learning/' + ' ')

@login_required
def learning_delete(request, id):
    learningskill = LearningSkill.objects.get(id=id)
    learningskill.delete()
    messages.info(request, 'deleted')
    return redirect('/learning/' + ' ')

#-----------------------------------------------------------------------------------------------#
# COMPETENCE - Sharing Skills
#-----------------------------------------------------------------------------------------------#

@login_required
def sharing(request, selected_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, ' ', selected_cycle, 'financial')
    alert = get_message(request)
    try:
        sharing_skills = SharingSkill.objects.filter(associate_id = request.user, created_date__range=[startdate, enddate])
        opencount = SharingSkill.objects.filter(associate_id = request.user, status = 'Open', created_date__range=[startdate, enddate]).count()
    except:
        sharing_skills = {}
        opencount = 0
    
    context = {
        'login' : login,
        'sharing_skills': sharing_skills,
        'opencount': opencount,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'alert': alert
        }
    return render(request, 'competence/sharing.html', context)

@login_required
def sharing_add(request):
    sharing_skill = SharingSkill()
    sharing_skill.associate_id = request.user
    sharing_skill.skill_title = request.POST['skill_title']
    sharing_skill.course_title = request.POST['course_title']
    sharing_skill.skill_duration_hours = request.POST['skill_duration_hours']
    sharing_skill.outcome_deliverable = request.POST['outcome_deliverable']
    sharing_skill.mode_of_activity = request.POST['mode_of_activity']
    sharing_skill.created_date = request.POST['created_date']
    sharing_skill.associates_trained = request.POST['associates_trained']
    sharing_skill.status = request.POST['status_add']
    sharing_skill.created_by = request.user
    sharing_skill.save()
    messages.info(request, 'added')
    sharing_skills = SharingSkill.objects.all()
    context = {'sharing_skills': sharing_skills}
    return redirect('/sharing/' + ' ')

@login_required
def sharing_update(request, id):
    sharing_skill = SharingSkill.objects.get(id=id)
    sharing_skill.skill_title = request.POST['skill_title']
    sharing_skill.course_title = request.POST['course_title']
    sharing_skill.skill_duration_hours = request.POST['skill_duration_hours']
    sharing_skill.outcome_deliverable = request.POST['outcome_deliverable']
    sharing_skill.mode_of_activity = request.POST['mode_of_activity']
    sharing_skill.created_date = request.POST['created_date']
    sharing_skill.associates_trained = request.POST['associates_trained']
    sharing_skill.status = request.POST['status_edit']
    sharing_skill.last_updated_by = str(request.user)
    sharing_skill.save()
    if request.POST['status_edit'] == 'Open':
        messages.info(request, 'updated')
    else:
        messages.info(request, 'submitted')
    return redirect('/sharing/' + ' ')

@login_required
def sharing_delete(request, id):
    sharingskill = SharingSkill.objects.get(id=id)  
    sharingskill.delete()
    messages.info(request, 'deleted')
    return redirect('/sharing/' + ' ')

#-----------------------------------------------------------------------------------------------#
# COMPETENCE - Technical & Managerial Skills
#-----------------------------------------------------------------------------------------------#

@login_required
def technical_managerial(request, selected_cycle):
    context = competence_transactions(request, selected_cycle, 'Competence', 'rating_technical_managerial')
    return render(request, 'competence/technical_managerial.html', context)

@login_required
def technical_managerial_update(request, effective_end):
    competence_transactions_update(request, effective_end, 'Competence')
    return redirect('/technical_managerial/' + ' ')

#-----------------------------------------------------------------------------------------------#
# COMPETENCE - Workplace Behaviour Skills
#-----------------------------------------------------------------------------------------------#

@login_required
def workplace_behaviour(request, selected_cycle):
    context = competence_transactions(request, selected_cycle, 'Organization Value', 'rating_workplace_behaviour')
    return render(request, 'competence/workplace_behaviour.html', context)

@login_required
def workplace_behaviour_update(request, effective_end):
    competence_transactions_update(request, effective_end, 'Organization Value')
    return redirect('/workplace_behaviour/' + ' ')

#-----------------------------------------------------------------------------------------------#
# COMPETENCE - Organization Development
#-----------------------------------------------------------------------------------------------#

@login_required
def organization_development(request, selected_cycle):
    context = competence_transactions(request, selected_cycle, 'Organization Development', 'rating_org_development')
    return render(request, 'competence/organization_development.html', context)

@login_required
def organization_development_update(request, effective_end):
    competence_transactions_update(request, effective_end, 'Organization Development')
    return redirect('/organization_development/' + ' ')

#-----------------------------------------------------------------------------------------------#
# COMPETENCE - Common for Technical & Managerial, Workplace Behaviour and Org Development
#-----------------------------------------------------------------------------------------------#

def competence_transactions(request, selected_cycle, category_type, rating_type):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, ' ', selected_cycle, 'financial')
    alert = get_message(request)
    try:
        categories = CompetenceTransaction.objects.filter(associate_id = request.user, category_type = category_type, effective_end__range=[startdate, enddate]).order_by('category')
        avg_rating = get_rating(request.user, selected_cycle, rating_type)
    except:
        categories = {}
        avg_rating = 0

    context = {
        'login' : login,
        'categories': categories,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'avg_rating': avg_rating,
        'alert': alert
        }
    return context

def competence_transactions_update(request, effective_end, category_type):
    skills = CompetenceTransaction.objects.filter(associate_id = request.user, effective_end = effective_end, category_type = category_type).order_by('category')
    for updateskill in skills:
        updateskill = CompetenceTransaction.objects.get(associate_id = request.user, effective_end = effective_end, category = updateskill.category)
        updateskill.associate_comments = request.POST[updateskill.category + '_' + "associate_comments"]
        updateskill.status = request.POST['status']
        updateskill.last_updated_by = str(request.user)
        updateskill.save()
    if request.POST['status'] == 'Open':
        messages.info(request, 'updated')
    else:
        messages.info(request, 'submitted')
    return 

#-----------------------------------------------------------------------------------------------#
# COMPETENCE - Productivity JIRA
#-----------------------------------------------------------------------------------------------#

@login_required
def productivity(request):
    login = Associate.objects.get(associate_id = request.user)
    jiras = Jira.objects.filter(jira_id = login.jira_id).order_by('-year', '-month')
    context = {
        'login' : login,
        'jiras': jiras
    }
    return render(request, 'competence/productivity.html', context)

#-----------------------------------------------------------------------------------------------#
# COMPETENCE - Descriptions
#-----------------------------------------------------------------------------------------------#

@login_required
def descriptions(request):
    template = loader.get_template('competence/descriptions.html')
    login = Associate.objects.get(associate_id = request.user)
    context = {
        'login' : login}
    return HttpResponse(template.render(context, request))

#-----------------------------------------------------------------------------------------------#
# WORK PERFORMANCE - Key Result Areas
#-----------------------------------------------------------------------------------------------#

@login_required
def associate_kras(request, selected_cycle, performance_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, ' ', selected_cycle, 'financial')
    alert = get_message(request)

    # To decide which ratings to be displayed
    cycle, performance_cycle, startdate, enddate, effective_end = get_misc_data(selected_cycle, performance_cycle)
    try:        
        categories = KraCategoryTransaction.objects.filter(associate_id = request.user, effective_end = effective_end) \
                                                    .exclude(status='Create') \
                                                    .order_by('category').extra(
                                                    select = {'project_name':
                                                    'Select project_name from Associate where '
                                                    'Associate.associate_id = KraCategoryTransaction.associate_id ',
                                                    'description':
                                                    'Select description from MasterCategory where '
                                                    'MasterCategory.category = KraCategoryTransaction.category '})
        
        if performance_cycle == 'cycle1' :
            rating_type = 'rating_kra_cycle1'
        elif performance_cycle == 'cycle2' :
            rating_type = 'rating_kra_cycle2'
        elif performance_cycle == 'cycle3' :
            rating_type = 'rating_kra_cycle3'
        avg_rating = get_rating(request.user, selected_cycle, rating_type)

        # for category in categories:
        #     subcategories = KraSubCategoryTransaction.objects.filter(associate_id = request.user, created_date__range=[startdate, enddate]) \
        #                                                      .exclude(status='Create') \
        #                                                      .order_by('subcategory')
                   
    except:
        categories = {}
        avg_rating = 0
        # subcategories = {}
    context = {
        'login' : login,
        'categories': categories,
        # 'subcategories': subcategories,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'performance_cycle': performance_cycle,
        'cycle': cycle,
        'avg_rating': avg_rating,
        'alert': alert
        }
    return render(request, 'workperformance/associate_kras.html', context)

@login_required
def associate_kras_update(request, effective_end):
    kracategories = KraCategoryTransaction.objects.filter(associate_id = request.user, effective_end = effective_end)
    for kracategory in kracategories:
        updatekracategory = KraCategoryTransaction.objects.get(associate_id = request.user, category = kracategory.category, effective_end = effective_end)
        updatekracategory.associate_comments = request.POST[kracategory.category + '_' + "associate_comments"]
        updatekracategory.status = request.POST['status']
        updatekracategory.last_updated_by = str(request.user)
        updatekracategory.save()

        # krasubcategories = KraSubCategoryTransaction.objects.filter(associate_id = request.user, category = kracategory.category, effective_end = effective_end)
        # for krasubcategory in krasubcategories:
        #     updatesubkracategory = KraSubCategoryTransaction.objects.get(associate_id = request.user, subcategory = krasubcategory.subcategory, effective_end = effective_end)
        #     updatesubkracategory.associate_comments = request.POST[krasubcategory.subcategory + '_' + "associate_comments"]
        #     updatesubkracategory.last_updated_by = str(request.user)
        #     updatesubkracategory.save()
    performance_cycle = request.POST['performance_cycle']
    selected_cycle = request.POST['selected_cycle']
    if request.POST['status'] == 'Open':
        messages.info(request, 'updated')
    else:
        messages.info(request, 'submitted')
    return redirect('/associate_kras/' + selected_cycle + '/' + performance_cycle)

#-----------------------------------------------------------------------------------------------#
# WORK PERFORMANCE - Awards Received
#-----------------------------------------------------------------------------------------------#

@login_required
def awards_received(request, selected_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, ' ', selected_cycle, 'financial')
    alert = get_message(request)
    try:
        awards = Award.objects.filter(associate_id = request.user, created_date__range=[startdate, enddate])
        opencount = Award.objects.filter(associate_id = request.user, status = 'Open', created_date__range=[startdate, enddate]).count()
    except:
        awards = {}
        opencount = 0

    award_categories = []
    categories = Category.objects.filter(category_type = 'Award')
    for category in categories:
        award_categories.append(category.category)

    context = {
        'login' : login,
        'awards': awards,
        'award_categories': award_categories,
        'opencount': opencount,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'alert': alert
    }
    return render(request, 'workperformance/awards_received.html', context)

@login_required
def awards_add(request):
    award = Award()
    award.associate_id = request.user
    award.award_category = request.POST['award_category']
    award.comments = request.POST['comments']
    award.created_date = request.POST['created_date']
    award.status = request.POST['status_add']
    award.created_by = request.user
    award.save()
    messages.info(request, 'added')
    awards = Award.objects.all()
    context = {'awards': awards}
    return redirect('/awards_received/' + ' ')

@login_required
def awards_update(request, id):
    rec = Award.objects.get(id=id)
    try:
        Page = request.POST['page']
        rec.status = 'Approved'
        rec.last_updated_by = str(request.user)
        rec.save()
        messages.info(request, 'approved')
        if request.POST['page'] == "pending_approvals":
            return redirect('/pending_approvals/' + 'awards')
        else:
            return redirect('/manager_awards/' + rec.associate_id + '/' + ' ')
    except:
        rec = Award.objects.get(id=id)
        rec.award_category = request.POST['award_category']
        rec.comments = request.POST['comments']
        rec.created_date = request.POST['created_date']
        rec.status = request.POST['status_edit']
        rec.last_updated_by = str(request.user)
        rec.save()
        if request.POST['status_edit'] == 'Open':
            messages.info(request, 'updated')
        else:
            messages.info(request, 'submitted')
        return redirect('/awards_received/' + ' ')

@login_required
def awards_delete(request, id):
    rec = Award.objects.get(id=id)
    rec.delete()
    messages.info(request, 'deleted')
    try:
        Page = request.POST['page']
        if Page == 'manager_awards':
            return redirect('/manager_awards/' + rec.associate_id + '/' + ' ')
        elif request.POST['page'] == "pending_approvals":
            return redirect('/pending_approvals/' + 'awards')
    except:
        return redirect('/awards_received/' + ' ')

#-----------------------------------------------------------------------------------------------#
# WORK PERFORMANCE - Appreciations Received
#-----------------------------------------------------------------------------------------------#

@login_required
def appreciations_received(request, selected_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, ' ', selected_cycle, 'financial')
    alert = get_message(request)
    try:
        commitments = Commitment.objects.filter(associate_id = request.user, status = 'Approved', created_date__range=[startdate, enddate]).extra(
                        select={'associate_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Commitment.associate_id',
                                'project_name':
                                'Select project_name from Associate where '
                                'Associate.associate_id = Commitment.associate_id',
                                'recognised_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Commitment.recognised_by',
                                'approval_required_by_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Commitment.approval_required_by'}
                                )
    except:
        commitments = {}
    
    context = {
        'login' : login,
        'commitments': commitments,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'alert': alert
    }
    return render(request, 'workperformance/appreciations_received.html', context)
    
#-----------------------------------------------------------------------------------------------#
# WORK PERFORMANCE - Appreciations Given
#-----------------------------------------------------------------------------------------------#

@login_required
def appreciations_given(request, selected_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, ' ', selected_cycle, 'financial')
    alert = get_message(request)
    associates = Associate.objects.filter(manager_id = request.user).order_by('associate_name')
    try:
        commitments = Commitment.objects.filter(recognised_by = request.user, created_date__range=[startdate, enddate]).extra(
                        select={'associate_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Commitment.associate_id',
                                'project_name':
                                'Select project_name from Associate where '
                                'Associate.associate_id = Commitment.associate_id',
                                'recognised_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Commitment.recognised_by',
                                'approval_required_by_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Commitment.approval_required_by'}
                                ).order_by('-created_date')
        opencount = Commitment.objects.filter(recognised_by = request.user, created_date__range=[startdate, enddate], status = 'Open').count()
    except:
        commitments = {}
        opencount = 0

    context = {
        'login' : login,
        'commitments': commitments,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'associates': associates,
        'opencount': opencount,
        'alert': alert
    }
    return render(request, 'workperformance/appreciations_given.html', context)

@login_required
def appreciations_add(request):
    associate = request.POST['recognise']
    Action = request.POST['action']
    if Action == "Save":
        status = "Open"
    else:
        status = "Pending Approval"
    try:
        associate_id = associate[associate.find('(')+1:associate.find(')')]
        associate = Associate.objects.get(associate_id = associate_id)
        rec = Commitment(
            project_id = associate.project_id,
            associate_id = associate_id,
            created_date = request.POST['created_date'],
            status = status,
            approval_required_by = associate.manager_id,
            recognised_by = request.user,
            created_by = request.user,
            comments = request.POST['comments'])
        rec.save()
        messages.info(request, 'added')
    except:
        pass
    return redirect('/appreciations_given/' + ' ')

@login_required        
def appreciations_update(request, trans_id):
    Page = request.POST['page']
    rec = Commitment.objects.get(id=trans_id)
    if Page == 'appreciations_given':
        Action = request.POST['action']
        if Action == "Save":
            rec.status = "Open"
            rec.created_date = request.POST['created_date']
        else:
            rec.status = "Pending Approval"
            rec.created_date = request.POST['created_date']
        rec.comments = request.POST['comments']
        rec.last_updated_by = str(request.user)
        rec.save()
        messages.info(request, 'updated')
        return redirect('/appreciations_given/' + ' ')
    else:
        rec.status = 'Approved'
        rec.last_updated_by = str(request.user)
        rec.save()
        messages.info(request, 'approved')
        if request.POST['page'] == "pending_approvals":
            return redirect('/pending_approvals/' + 'appreciations')
        else:
            return redirect('/manager_appreciations/' + rec.associate_id + '/' +' ')

@login_required
def appreciations_delete(request, trans_id):
    Page = request.POST['page']
    rec = Commitment.objects.get(id=trans_id)
    rec.delete()
    messages.info(request, 'deleted')
    if Page == 'appreciations_given':
        return redirect('/appreciations_given/' + ' ')
    elif request.POST['page'] == "pending_approvals":
        return redirect('/pending_approvals/' + 'appreciations')
    else:
        return redirect('/manager_appreciations/' + rec.associate_id + '/' + ' ')

#-----------------------------------------------------------------------------------------------#
# WORK PERFORMANCE - Feedback Received
#-----------------------------------------------------------------------------------------------#

@login_required
def feedback_received(request, selected_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, ' ', selected_cycle, 'financial')
    alert = get_message(request)
    try:
        feedbacks = Feedback.objects.filter(associate_id = request.user, created_date__range=[startdate, enddate]).extra(
                        select={'feedback_by_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Feedback.associate_id',
                                'approval_required_by_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Feedback.approval_required_by'}
                                )
        inprogresscount = Feedback.objects.filter(associate_id = request.user, created_date__range=[startdate, enddate], status = "In Progress").count()
    except:
        feedbacks = {}
        inprogresscount = 0

    context = {
        'login' : login,
        'feedbacks': feedbacks,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'inprogresscount': inprogresscount,
        'alert': alert
    }
    return render(request, 'workperformance/feedback_received.html', context)

#-----------------------------------------------------------------------------------------------#
# WORK PERFORMANCE - Feedback Given
#-----------------------------------------------------------------------------------------------#

@login_required
def feedback_given(request, selected_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, ' ', selected_cycle, 'financial')
    alert = get_message(request)
    associates = Associate.objects.filter(manager_id = request.user).order_by('associate_name')
    categories = MasterCategory.objects.all()
    try:
        feedbacks = Feedback.objects.filter(feedback_by = request.user, created_date__range=[startdate, enddate]).extra(
                        select={'associate_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Feedback.associate_id',
                                'approval_required_by_name':
                                'Select associate_name from Associate where '
                                'Associate.associate_id = Feedback.approval_required_by'}
                                )
        opencount = Feedback.objects.filter(feedback_by = request.user, created_date__range=[startdate, enddate], status = 'Open').count()
    except:
        feedbacks = {}
        opencount = 0

    context = {
        'login' : login,
        'feedbacks': feedbacks,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'associates': associates,
        'categories': categories,
        'opencount': opencount,
        'alert': alert
    }
    return render(request, 'workperformance/feedback_given.html', context)

@login_required
def feedback_add(request):
    associate = request.POST['recognise']
    Action = request.POST['action']
    if Action == "Save":
        status = "Open"
    else:
        status = "Pending Approval"
    associate_id = associate[associate.find('(')+1:associate.find(')')]
    associate = Associate.objects.get(associate_id = associate_id)
    rec = Feedback(
        project_id= associate.project_id,
        associate_id=associate_id,
        category=request.POST['category'],
        status = status,
        created_date = request.POST['created_date'],
        approval_required_by = associate.manager_id,
        feedback_by = request.user,
        created_by = request.user,
        comments = request.POST['comments'],
        action_point = request.POST['actionpoints'])
    rec.save()
    messages.info(request, 'added')
    return redirect('/feedback_given/' + ' ')

@login_required
def feedback_update(request, feed_id):
    Page = request.POST['page']
    Action = request.POST['action']
    rec = Feedback.objects.get(id=feed_id)
    rec.last_updated_by = str(request.user)
    if Page == 'feedback_given':
        if Action == "Send for Approval":
            rec.status = "Pending Approval"
        rec.category = request.POST['category']
        rec.comments = request.POST['comments']
        rec.created_date = request.POST['created_date']
        rec.action_point = request.POST['actionpoints']
        rec.save()
        messages.info(request, 'updated')
        return redirect('/feedback_given/' + ' ')
    elif Page =='feedback_received':
        rec.action_implemented = request.POST['action_implemented']
        rec.created_date = request.POST['created_date']
        if Action == "Send for Approval":
            rec.status = "Action Approval"
        rec.save()
        messages.info(request, 'updated')
        return redirect('/feedback_received/' + ' ') 
    else:
        rec.supervisor_comments = request.POST['supervisorcomments']
        rec.created_date = request.POST['created_date']
        if Action == "Approve":
            if rec.status == "Pending Approval":
                rec.status = "In Progress"
            elif rec.status == "Action Approval":
                rec.status = "Close"
            rec.save()
            messages.info(request, 'updated')
        elif Action == "Reject":
            if rec.status == "Pending Approval":
                feedback_delete(request, feed_id)
                messages.info(request, 'deleted')
            elif rec.status == "Action Approval":
                rec.status = "In Progress"
                rec.save()
                messages.info(request, 'updated')
        elif Action == "Save":
            rec.save()
            messages.info(request, 'updated')
        if Page == 'manager_feedback':
            return redirect('/manager_feedback/' + rec.associate_id  + '/' + ' ')
        elif Page == "pending_approvals":
            return redirect('/pending_approvals/' + 'feedback')
        else:
            return HttpResponseRedirect(reverse('competence:feedback_pending'))

@login_required
def feedback_delete(request, feed_id):
    Page = request.POST['page']
    rec = Feedback.objects.get(id=feed_id)
    associate_id = rec.associate_id
    rec.delete()
    messages.info(request, 'deleted')
    if Page == 'feedback_given':
        return redirect('/feedback_given/' + ' ')
    elif Page == 'manager_feedback':
            return redirect('/manager_feedback/' + rec.associate_id  + '/' + ' ')
    elif Page == "pending_approvals":
        return redirect('/pending_approvals/' + 'feedback')
    else:
        return HttpResponseRedirect(reverse('competence:feedback_pending'))

#-----------------------------------------------------------------------------------------------#
# WORK PERFORMANCE - Performance Rating
#-----------------------------------------------------------------------------------------------#

@login_required
def performance_rating(request, selected_cycle, performance_cycle):
    associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles = get_details(request, ' ', selected_cycle, 'financial')

    # To decide which ratings to be displayed
    cycle, performance_cycle, startdate, enddate, effective_end = get_misc_data(selected_cycle, performance_cycle)

    # COUNTS
    learning_count = LearningSkill.objects.filter(associate_id = request.user, status = 'Close', created_date__range=[startdate, enddate]).count()
    sharing_count = SharingSkill.objects.filter(associate_id = request.user, status = 'Close', created_date__range=[startdate, enddate]).count()
    awards_count = Award.objects.filter(associate_id = request.user, created_date__range=[startdate, enddate], status = 'Approved').count()
    appreciations_count = Commitment.objects.filter(associate_id = request.user, created_date__range=[startdate, enddate], status = 'Approved').count()
    status = ['In Progress', 'Action Approval', 'Close']
    feedbacks = Feedback.objects.filter(associate_id = request.user, created_date__range=[startdate, enddate], status__in = status)
    feedbacks_count = feedbacks.count()

    # RATINGS
    try:
        rating = Ratings.objects.get(associate_id = request.user, cycle = selected_cycle)
    except:
        rating = {}
        
    context = {
        'login' : login,
        'rating': rating,
        'cycles': cycles,
        'selected_cycle': selected_cycle,
        'performance_cycle': performance_cycle,
        'learning_count': learning_count,
        'sharing_count': sharing_count,
        'awards_count': awards_count,
        'appreciations_count': appreciations_count,
        'feedbacks_count' :feedbacks_count,
        'feedbacks': feedbacks,
        'cycle': cycle,
    }

    return render(request, 'workperformance/performance_rating.html', context)

#-----------------------------------------------------------------------------------------------#
# CALCULATE RATINGS
#-----------------------------------------------------------------------------------------------#

def get_rating(associate_id, cycle, rating_type):
    try:
        rating = Ratings.objects.get(associate_id = associate_id, cycle = cycle)
        rating_value = getattr(rating, rating_type)
    except:
        rating_value = None
    return rating_value

def set_rating(request, associate_id, cycle, rating_type):
    try:
        # Update the record
        rec = Ratings.objects.get(associate_id = associate_id, cycle = cycle)
        setattr(rec, rating_type, request.POST['final_rating'])
        rec.last_updated_by = str(request.user)
        rec.save()
    except:
        # Add a new record
        rec = Ratings()
        rec.associate_id = associate_id
        rec.cycle = cycle
        rec.created_by = str(request.user)
        setattr(rec, rating_type, request.POST['final_rating'])
        rec.save()

#-----------------------------------------------------------------------------------------------#
# REUSABLE FUNCTIONS
#-----------------------------------------------------------------------------------------------#

# Get the Current Financial Cycle
def get_current_financial_cycle():
    if datetime.now().month > 3:
        selected_cycle = 'FY ' + str(datetime.now().year) + ' - ' + str(datetime.now().year + 1)
    else:
        selected_cycle = 'FY ' + str(datetime.now().year - 1) + ' - ' + str(datetime.now().year)
    return selected_cycle

# Get the previous Financial Cycles
def get_financial_cycles():
    financial_cycles = []
    for i in range(3):
        if datetime.now().month > 3:
            financial_cycle = 'FY ' + str(datetime.now().year - i) + ' - ' + str(datetime.now().year - i + 1)
        else:
            financial_cycle = 'FY ' + str(datetime.now().year - i - 1) + ' - ' + str(datetime.now().year - i)
        financial_cycles.append(financial_cycle)
    return financial_cycles

# Get the Current ADR Cycle
def get_current_adr_cycle():
    if datetime.now().month >=3 and datetime.now().month <=7:
        selected_cycle = 'Apr-' + str(datetime.now().year)[2:] + ' to ' 'Jul-' + str(datetime.now().year)[2:]
    elif datetime.now().month >=8 and datetime.now().month <=11:
        selected_cycle = 'Aug-' + str(datetime.now().year)[2:] + ' to ' 'Nov-' + str(datetime.now().year)[2:]
    else:
        selected_cycle = 'Dec-' + str(datetime.now().year)[2:] + ' to ' 'Mar-' + str(datetime.now().year+1)[2:]
    return selected_cycle

# Get the previous ADR Cycles
def get_adr_cycles():
    current_month = datetime.now().month
    current_year = datetime.now().year
    cycles = []
    for i in range(9):
        if current_month >3 and current_month <=7:
            cycles.append('Apr-' + str(current_year)[2:] + ' to ' 'Jul-' + str(current_year)[2:])
        elif current_month >7 and current_month <=10:
            cycles.append('Aug-' + str(current_year)[2:] + ' to ' 'Nov-' + str(current_year)[2:])
        else:
            cycles.append('Dec-' + str(current_year-1)[2:] + ' to ' 'Mar-' + str(current_year)[2:])
            current_year = current_year - 1
        current_month = current_month - 4
        if current_month <= 0:
            current_month = 12 + current_month     
    return cycles

def get_date_range(selected_cycle):
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    startmonth = int(months.index(selected_cycle[:3]) + 1) 
    endmonth = int(months.index(selected_cycle[10:13]) + 1)
    LastDay = calendar.monthrange(int(selected_cycle[14:16]), endmonth)[1]
    startmonth = '0' + str(startmonth) if startmonth < 10 else str(startmonth)
    endmonth = '0' + str(endmonth) if endmonth < 10 else str(endmonth)
    startdate = '20' + str(selected_cycle[4:6]) + '-' + startmonth + '-01' 
    enddate = '20' + selected_cycle[14:16] + '-' + endmonth + '-' + str(LastDay)
    return startdate, enddate

def get_selected_date_range(selectedMonth):
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    Month = int(months.index(selectedMonth[:3]) + 1) # Substring the month to get the Month number. Eg Jan - 01
    Year = int(selectedMonth[5:]) # Substring the Year
    LastDay = calendar.monthrange(Year,Month)[1] # Get the last day of the selected Month and Year

    if Month >=10:
        ToDate = str(Year) +'-'+ str(Month) +'-'+ str(LastDay) + ' 11:59'
        FromDate = str(Year) +'-' +str(Month) +'-01' + ' 00:00'
    else:
        ToDate = str(Year) +'-0'+ str(Month) +'-'+ str(LastDay) + ' 11:59'
        FromDate = str(Year) +'-0' +str(Month) +'-01' + ' 00:00'
    return FromDate, ToDate

def get_pending_work_performance(associate_id, startdate, enddate):
    try:
        awards_pendingcount = Award.objects.filter(associate_id = associate_id, status = 'Pending Approval', created_date__range=[startdate, enddate]).count()
    except:
        awards_pendingcount = 0 
    try:
        appreciations_pendingcount = Commitment.objects.filter(associate_id = associate_id, status = 'Pending Approval', created_date__range=[startdate, enddate]).count()
    except:
        appreciations_pendingcount = 0
    try:
        status = ['Action Approval', 'Pending Approval']
        feedback_pendingcount = Feedback.objects.filter(associate_id = associate_id, status__in = status, created_date__range=[startdate, enddate]).count()
    except:
        feedback_pendingcount = 0
    return awards_pendingcount, appreciations_pendingcount, feedback_pendingcount

def get_pending_competence(associate_id, startdate, enddate):
    try:
        learning_pendingcount = LearningSkill.objects.filter(associate_id = associate_id, status = 'Pending Approval', created_date__range=[startdate, enddate]).count()
    except:
        learning_pendingcount = 0
    try:
        sharing_pendingcount = SharingSkill.objects.filter(associate_id = associate_id, status = 'Pending Approval', created_date__range=[startdate, enddate]).count()
    except:
        sharing_pendingcount = 0  
    return learning_pendingcount, sharing_pendingcount

def get_details(request, associate_id, selected_cycle, type):
    login = Associate.objects.get(associate_id = request.user)
    associates = Associate.objects.filter(manager_id = request.user).order_by('associate_name')
    if associate_id == ' ':
        try:
            selected_associate = Associate.objects.filter(manager_id = request.user).order_by('associate_name').first()
            associate_id = selected_associate.associate_id
            if not selected_associate:
                associate_id = str(request.user)   
        except:
            associate_id = str(request.user)
    associate_name = Associate.objects.get(associate_id = associate_id)

    # Get the last three financial cycles or last 12 ADR cycles
    if type == 'financial':
        if selected_cycle == ' ':
            selected_cycle = get_current_financial_cycle()
        startdate = selected_cycle[3:7] + '-03-31'
        enddate = selected_cycle[10:14] + '-04-01'
        cycles = get_financial_cycles()
    elif type == 'adr':
        if selected_cycle == ' ':
            selected_cycle = get_current_adr_cycle()
        startdate, enddate = get_date_range(selected_cycle)
        cycles = get_adr_cycles()
    return associate_id, associate_name, selected_cycle, login, associates, startdate, enddate, cycles

def get_misc_data(selected_cycle, performance_cycle):    
    adr_cycle = get_current_adr_cycle()
    if selected_cycle == get_current_financial_cycle():
        if adr_cycle[:3] == 'Apr':
            cycle = 1
        elif adr_cycle[:3] == 'Aug':
            cycle = 2
        elif adr_cycle[:3] == 'Dec':
            cycle = 3
    else:
        cycle = 3
    
    # Calculate Start Date and End Date based on the selected Performance Cycle
    if performance_cycle == 'cycle1':
        startdate = selected_cycle[3:7] + '-03-31'
        enddate = selected_cycle[3:7] + '-08-01'
        effective_end = selected_cycle[3:7] + '-07-31'

    elif performance_cycle == 'cycle2':
        startdate = selected_cycle[3:7] + '-07-31'
        enddate = selected_cycle[3:7] + '-12-01'
        effective_end = selected_cycle[3:7] + '-11-30'

    elif performance_cycle == 'cycle3':
        startdate = selected_cycle[3:7] + '-11-30'
        enddate = selected_cycle[10:14] + '-04-01'
        effective_end = selected_cycle[10:14] + '-03-31'

    elif performance_cycle == 'annual':
        startdate = selected_cycle[3:7] + '-03-31'
        enddate = selected_cycle[10:14] + '-04-01'
        effective_end = selected_cycle[10:14] + '-03-31'

    elif performance_cycle == ' ':
        if adr_cycle[:3] == 'Apr': 
            startdate = selected_cycle[3:7] + '-03-31'
            enddate = selected_cycle[3:7] + '-08-01'
            effective_end = selected_cycle[3:7] + '-07-31'
            performance_cycle = 'cycle1'

        elif adr_cycle[:3] == 'Aug':
            startdate = selected_cycle[3:7] + '-07-31'
            enddate = selected_cycle[3:7] + '-12-01'
            effective_end = selected_cycle[3:7] + '-11-30'
            performance_cycle = 'cycle2'

        elif adr_cycle[:3] == 'Dec':
            startdate = selected_cycle[3:7] + '-11-30'
            enddate = selected_cycle[10:14] + '-04-01'
            effective_end = selected_cycle[10:14] + '-03-31'
            performance_cycle = 'cycle3'

    return cycle, performance_cycle, startdate, enddate, effective_end

def get_message(request):
    alert ="null"
    storage = get_messages(request)
    for message in storage:
        alert = str(message)
    return alert
