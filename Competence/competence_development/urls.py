from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'competence'
urlpatterns = [

    # ----------------------------------------------------------------------------------------
    # USER LOGIN
    # ----------------------------------------------------------------------------------------
    path('', views.user_login, name ='user_login'),
    path('change_password/', views.change_password, name ='change_password'),
    path('update_password/', views.update_password, name ='update_password'),
    path('logout/', views.user_logout, name ='user_logout'),

    # ----------------------------------------------------------------------------------------
    # MENU - DASHBOARD
    # ----------------------------------------------------------------------------------------
    url(r'^dashboard$', views.dashboard, name='dashboard'),
    url(r'^mmc_payment_status/(?P<selected_month>[\&\-\_\w.@+-| ]+)$', views.mmc_payment_status, name='mmc_payment_status'),
    url(r'^mmc_payment_status_update/(?P<selected_month>[\&\-\_\w.@+-| ]+)$', views.mmc_payment_status_update, name='mmc_payment_status_update'),
]