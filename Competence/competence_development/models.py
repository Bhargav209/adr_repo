from django.db import models
import datetime

#-----------------------------------------------------------------------------------------------#
# Customer Master
#-----------------------------------------------------------------------------------------------#

class CustomerMaster(models.Model):
    flat_no = models.CharField(max_length=3, null = True, blank = True)
    floor_no = models.IntegerField(max_length=2, null = True, blank = True)
    name = models.CharField(max_length=50, null = True, blank = True)
    email = models.CharField(max_length=50, null = True, blank = True)
    two_wheeler = models.CharField(max_length=1, null = True, blank = True)
    two_wheeler_no = models.CharField(max_length=10, null = True, blank = True)
    four_wheeler = models.CharField(max_length=1, null = True, blank = True)
    four_wheeler_no = models.CharField(max_length=10, null = True, blank = True)
    electricity_bill = models.CharField(max_length=10, null = True, blank = True)
    created_by = models.CharField(max_length=50, null = True, blank = True)
    created_date = models.DateTimeField(null=True, auto_now_add = True, auto_now = False, blank = True)
    last_updated_by = models.CharField(max_length=50, null = True, blank = True)
    last_updated_date = models.DateTimeField(null=True, auto_now_add = False, auto_now = True, blank = True)

    class Meta:
        db_table = 'CustomerMaster'

#-----------------------------------------------------------------------------------------------#
# Monthly Maintenance Table per Flat
#-----------------------------------------------------------------------------------------------#

class FlatMaintenance(models.Model):
    flat_no = models.CharField(max_length=3, null = True, blank = True)
    mmc_year = models.IntegerField(max_length=4, null = True, blank = True)
    mmc_month = models.IntegerField(max_length=2, null = True, blank = True)
    mmc_amount = models.FloatField(max_length=4, null = True, blank = True)
    method_of_payment = models.CharField(max_length=20, null = True, blank = True)
    created_by = models.CharField(max_length=50, null = True, blank = True)
    created_date = models.DateTimeField(null=True, blank = True)
    last_updated_by = models.CharField(max_length=50, null = True, blank = True)
    last_updated_date = models.DateTimeField(null=True, auto_now_add = False, auto_now = True, blank = True)

    class Meta:
        db_table = 'FlatMaintenance'

#-----------------------------------------------------------------------------------------------#
# Staff Maintenance Table
#-----------------------------------------------------------------------------------------------#

class StaffMaintenance(models.Model):
    year = models.IntegerField(max_length=4, null = True, blank = True)
    month = models.IntegerField(max_length=2, null = True, blank = True)
    total_amount = models.FloatField(max_length=6, null = True, blank = True)
    amount_per_flat = models.FloatField(max_length=4, null = True, blank = True)
    supervisor_count = models.IntegerField(max_length=2, null = True, blank = True)
    supervisor_cost = models.FloatField(max_length=4, null = True, blank = True)
    security_count = models.IntegerField(max_length=2, null = True, blank = True)
    security_cost = models.FloatField(max_length=4, null = True, blank = True)
    housekeeping_count = models.IntegerField(max_length=2, null = True, blank = True)
    housekeeping_cost = models.FloatField(max_length=4, null = True, blank = True)
    watchman_count = models.IntegerField(max_length=2, null = True, blank = True)
    watchman_cost = models.FloatField(max_length=4, null = True, blank = True)
    electrician_count = models.IntegerField(max_length=2, null = True, blank = True)
    electrician_cost = models.FloatField(max_length=4, null = True, blank = True)
    plumber_count = models.IntegerField(max_length=2, null = True, blank = True)
    plumber_cost = models.FloatField(max_length=4, null = True, blank = True)
    pool_cleaner_count = models.IntegerField(max_length=2, null = True, blank = True)
    pool_cleaner_cost = models.FloatField(max_length=4, null = True, blank = True)
    gardener_count = models.IntegerField(max_length=2, null = True, blank = True)
    gardener_cost = models.FloatField(max_length=4, null = True, blank = True)
    created_by = models.CharField(max_length=50, null = True, blank = True)
    created_date = models.DateTimeField(null=True, blank = True)
    last_updated_by = models.CharField(max_length=50, null = True, blank = True)
    last_updated_date = models.DateTimeField(null=True, auto_now_add = False, auto_now = True, blank = True)

    class Meta:
        db_table = 'StaffMaintenance'