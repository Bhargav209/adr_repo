from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader, RequestContext
import os, pdb
import sweetify
from datetime import datetime, timedelta
import numpy as np
import subprocess as sp
import MySQLdb 
import re
import time
import smtplib 
import threading

from operator import itemgetter
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect, reverse
from django.shortcuts import get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone

from twilio.rest import Client  
from django.views import generic
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.db.models import Count

from .models import CustomerMaster, FlatMaintenance

# from flask import Flask, request, render_template
from django.views.generic import TemplateView
from chartjs.views.lines import BaseLineChartView
from .fusioncharts import FusionCharts

# for login purpose
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Basic Libraries
import calendar
import math

# Google Speech to Text
import speech_recognition as sr

# Message info
from django.contrib.messages import get_messages
from django.contrib import messages

#-----------------------------------------------------------------------------------------------#
# USER LOGIN
#-----------------------------------------------------------------------------------------------#

def user_login(request):
    invalid = None
    active = None
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username = username, password = password)
        next = request.POST.get('next')
        if user:
            if user.is_active:
                login(request,user)
                if next:
                    return redirect('competence:dashboard')
                else:
                    return HttpResponseRedirect(reverse('competence:dashboard'))
            else:
                active = 'TRUE'
                context = {'next':next, 'invalid':invalid,'active':active}
                return render(request, 'registration/login.html', context )
        else:
            invalid = 'TRUE'
            print("Someone tried to login and failed.")
            print("They used username: {} and password: {}".format(username, password))
            context = {
                'next': next, 
                'invalid': invalid,
                'active': active}
            return render(request, 'registration/login.html', context)
    else:
        #Nothing has been provided for username or password.
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('competence:dashboard'))
        else:
            next = request.GET.get('next')
            context = {'next':next, 'invalid':invalid,'active':active}
            if next:
                return render(request, 'registration/login.html', context)
            else:
                return render(request, 'registration/login.html')
                
def change_password(request):
    return render(request, 'registration/change_password.html')

def update_password(request):
    invalid = None
    user = authenticate(username = request.POST['username'], password = request.POST['password'])
    if not user:
        error = 'Username or Password do not match'
        invalid = 'TRUE'
    elif request.POST['new_password'] == '' or request.POST['confirm_password'] == '':
        error = 'New Password or Confirm Password cannot be blanks'
        invalid = 'TRUE'
    elif request.POST['new_password'] != request.POST['confirm_password']:
        error = 'New Password and Confirm Password do not match'
        invalid = 'TRUE'

    if invalid:
        context = {
            'invalid': invalid,
            'error': error}
        return render(request, 'registration/change_password.html', context)
    else:
        # update the New Password
        user = User.objects.get(username = request.POST['username'])
        user.set_password(request.POST['new_password'])
        user.save()
        return HttpResponseRedirect(reverse('competence:user_login'))

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('competence:user_login'))

#-----------------------------------------------------------------------------------------------#
# MAIN MENU - DASHBOARD
#-----------------------------------------------------------------------------------------------#

@login_required 
def dashboard(request):
    context = {}
    return render(request, 'dashboard.html', context)

#-----------------------------------------------------------------------------------------------#
# MONTHLY MAINTENANCE PAYMENT STATUS
#-----------------------------------------------------------------------------------------------#

@login_required 
def mmc_payment_status(request, selected_month):
    if selected_month == ' ':
        selected_month = calendar.month_abbr[datetime.now().month] + ' - ' + str(datetime.now().year)

    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    Month = str(months.index(selected_month[:3]) + 1)
    Year = str(selected_month[5:])

    customers  = CustomerMaster.objects.all().order_by('-floor_no', 'flat_no').extra(
                    select = {'mop':
                    'Select method_of_payment from FlatMaintenance where '
                    'CustomerMaster.flat_no = FlatMaintenance.flat_no and '
                    'FlatMaintenance.mmc_year = ' + Year +  ' and ' 
                    'FlatMaintenance.mmc_month = ' + Month ,
                    'created_date':
                    'Select created_date from FlatMaintenance where '
                    'CustomerMaster.flat_no = FlatMaintenance.flat_no and '
                    'FlatMaintenance.mmc_year = ' + Year +  ' and ' 
                    'FlatMaintenance.mmc_month = ' + Month ,
                    'mmc_amount':
                    'Select mmc_amount from FlatMaintenance where '
                    'CustomerMaster.flat_no = FlatMaintenance.flat_no and '
                    'FlatMaintenance.mmc_year = ' + Year +  ' and ' 
                    'FlatMaintenance.mmc_month = ' + Month }
                    )
                     
    paymentList = []
    floors = ['G','1','2','3','4','5']
    for i in floors:
        paymentList.append(round(FlatMaintenance.objects.filter(flat_no__startswith = i, mmc_year = Year, mmc_month = Month).count()/14 * 100, 2))
    
    last12months = last_12_months()
    mops = ['Online' , 'Cash']
    context = {
        'customers': customers,
        'paymentList': paymentList,
        'last12months': last12months,
        'selected_month': selected_month,
        'mops': mops
    }
    return render(request, 'mmc_payment_status.html', context)

@login_required 
def mmc_payment_status_update(request, selected_month):
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    Month = str(months.index(selected_month[:3]) + 1)
    Year = str(selected_month[5:])
    rec = FlatMaintenance(
        flat_no = request.POST['flat_no'],
        mmc_year = int(Year),
        mmc_month = int(Month),
        mmc_amount = request.POST['mmc_amount'],
        method_of_payment = request.POST['mop'],
        created_date = request.POST['payment_date'],
        created_by = request.user)
    rec.save()
    return redirect('/mmc_payment_status/' + selected_month)
  
#-----------------------------------------------------------------------------------------------#
# AVAIBALE FLATS LIST
#-----------------------------------------------------------------------------------------------#  

def flats_list():
    flatsList = [
        ['501', '502', '503', '504', '505', '506', '507', '508', '509', '510', '511', '512', '513', '514'],
        ['401', '402', '403', '404', '405', '406', '407', '408', '409', '410', '411', '412', '413', '414'],
        ['301', '302', '303', '304', '305', '306', '307', '308', '309', '310', '311', '312', '313', '314'],
        ['201', '202', '203', '204', '205', '206', '207', '208', '209', '210', '211', '212', '213', '214'],
        ['101', '102', '103', '104', '105', '106', '107', '108', '109', '110', '111', '112', '113', '114'],
        ['G01', 'G02', 'G03', 'G04', 'G05', 'G06', 'G07', 'G08', 'G09', 'G10', 'G11', 'G12', 'G13', 'G14']           
                ]
    return flatsList

def last_12_months():
    currentMonth = datetime.now().month
    currentYear = datetime.now().year
    totalmonths = currentYear * 12 + currentMonth - 1 # Months since year 0 minus 1
    last12months = []
    for i in range(12):
        month = calendar.month_abbr[(totalmonths - i) % 12 + 1] + ' - ' + str((totalmonths - i) // 12)
        last12months.append(month)
    return last12months