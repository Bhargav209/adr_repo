from django.apps import AppConfig


class CompetenceDevelopmentConfig(AppConfig):
    name = 'competence_development'
