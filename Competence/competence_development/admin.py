from django.contrib import admin

from .models import CustomerMaster, FlatMaintenance, StaffMaintenance

@admin.register(CustomerMaster)
class CustomerMasterAdmin(admin.ModelAdmin):
    list_display = ('flat_no', 'name', 'email', 'two_wheeler_no', 'four_wheeler_no', 'electricity_bill')
    list_filter = ('floor_no', 'name', 'email')

@admin.register(FlatMaintenance)
class FlatMaintenanceAdmin(admin.ModelAdmin):
    list_display = ('flat_no', 'mmc_year', 'mmc_month', 'mmc_amount', 'method_of_payment', 'created_date')
    list_filter = ('flat_no', 'mmc_year', 'mmc_month')

@admin.register(StaffMaintenance)
class StaffMaintenanceAdmin(admin.ModelAdmin):
    list_display = ('year', 'month', 'total_amount', 'amount_per_flat', 'created_date')
    list_filter = ('year', 'month')
