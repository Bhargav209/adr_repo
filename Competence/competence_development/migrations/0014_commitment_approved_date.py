# Generated by Django 2.0.1 on 2018-08-17 19:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('competence_development', '0013_auto_20180817_1938'),
    ]

    operations = [
        migrations.AddField(
            model_name='commitment',
            name='approved_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
